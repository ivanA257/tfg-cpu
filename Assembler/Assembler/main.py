
destiny = {'none'     : '0000',
           'mdr'      : '0001',
           'mar'      : '0010',
           'x'        : '0011',
           'reg_1'    : '0100',
           'reg_2'    : '0101',
           'reg_3'    : '0110',
           'reg_4'    : '0111',
           'jump_reg' : '1000'}

origin = {'none'   : '0000',
          'mdr'    : '0001',
          'y'      : '0010',
          'reg_1'  : '0011',
          'reg_2'  : '0100',
          'reg_3'  : '0101',
          'reg_4'  : '0110',
          'custom' : '0111'}

alu_op_list = {'zero' : '101010',
               'one'  : '111111',
               'mone' : '111010',
               'getx' : '001100',
               'gety' : '110000',
               'negx' : '001101',
               'negy' : '110001',
               'mx'   : '001111',
               'my'   : '110011',
               'incx' : '011111',
               'incy' : '110111',
               'decx' : '001110',
               'decy' : '110010',
               'add'  : '000010',
               'sub'  : '010011',
               'msub' : '000111',
               'and'  : '000000',
               'or'   : '010101'}


jump_list = {'njump' : '000', # Null jump
             'jgt'   : '001',
             'jeq'   : '010',
             'jge'   : '011',
             'jlt'   : '100',
             'jne'   : '101',
             'jle'   : '110',
             'jmp'   : '111'}

def decode_set(words, k):
    if not words[1].isnumeric():
        raise Exception("The introduced number after the set instruction was not valid. Check line: {}".format(k))
    else:
        num = int(words[1])
        binary_num = "{:015b}".format(num)
        return '1' + binary_num + "0000"

def decode_mov(words, k):
    if len(words) != 3:
        raise Exception("The number of words is other than 3. Check line: {}".format(k))
    elif words[1] not in origin.keys():
        raise Exception("The origin keyword is not valid. Check line: {}".format(k))
    elif words[2] not in destiny.keys():
        raise Exception("The destiny keyword is not valid. Check line: {}".format(k))
    else:
        origin_code = origin[words[1]]
        destiny_code = destiny[words[2]]
        # If the destination is mdr we need to make m_load = 1, because otherwise mdr is reading the memory, not the bus
        if words[2] == 'mdr':
            return "01{}{}1000000000".format(origin_code, destiny_code)
        else:
            return "01{}{}0000000000".format(origin_code, destiny_code)


def decode_alu(words, k):
    if words[1] not in origin.keys():
        raise Exception("The origin keyword is not valid. Check line: {}".format(k))
    else:
        alu_code = alu_op_list[words[0]]
        origin_code = origin[words[1]]
        return "00{}00000{}000".format(origin_code, alu_code)

def decode_jump(words, k):
    if words[1] not in origin.keys():
        raise Exception("The origin keyword is not valid. Check line: {}".format(k))
    else:
        jump_code = jump_list[words[0]]
        origin_code = origin[words[1]]
        alu_code = "110000" # Because we want to take the value from the bus.
        return "00{}00000{}{}".format(origin_code, alu_code, jump_code)




test_format = 'memory'
path = 'assembler.asm'
mach_path = 'mach.m'
mach_code = []
line_count = 1
with open(path, 'r') as file:
    for line in file:
        # First of all we'll clean up the comments.
        comment_index = line.find("//")
        if comment_index != -1:
            line = line[:comment_index]
        words = line.split()
        if words[0] == 'set':
            mach_line = decode_set(words, line_count)
        elif words[0] == 'mov':
            mach_line = decode_mov(words, line_count)
        elif words[0] in alu_op_list.keys():
            mach_line = decode_alu(words, line_count)
        elif words[0] == 'm_load':
            mach_line = '00000000001000000000'
        elif words[0] in jump_list.keys():
            mach_line = decode_jump(words, line_count)
        else:

            raise Exception("Sorry but the operation introduced is not a legal one. Check line: {}".format(line_count))
        mach_code.append(mach_line + '\n')
        line_count += 1

line_count = 1
with open(mach_path, 'w') as file:
    for line in mach_code:
        line = "000000000000" + line # This completes the remaining zeroes from the left, cause we need 32 bits.
        # This formatting allows us to directly paste it into the vhdl code.
        if test_format == 'vhdl':
            to_write = 'instruction <= "{}";\nwait for TbPeriod;\n'.format(line.strip())
            file.write(to_write)
        elif test_format == 'memory':
            instr = '{} => "{}",\n'.format(line_count-1, line.strip())
            if line_count == 1:
                to_memory = '    signal ram : ram_type := (' + instr
            else:
                to_memory = '                              ' + instr
            file.write(to_memory)
        else:
            file.write(line)
        line_count += 1


