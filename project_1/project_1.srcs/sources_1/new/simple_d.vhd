library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity simple_d is
    port (
        d : in std_logic;        
        clk : in std_logic;
        chipsel : in std_logic;
        q : out std_logic
    );
end entity simple_d;

architecture behav of simple_d is

    

begin

    process (clk)
    begin
        if rising_edge (clk) then
            if chipsel = '1' then
                q <= d;
            end if;
        end if;
    end process;

end behav;
    
