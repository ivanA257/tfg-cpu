library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_buttons is
Port (
    clk      : in std_logic;
    increase : in std_logic;
    decrease : in std_logic;
    output   : out std_logic_vector(6 downto 0)
 );
end counter_buttons;

architecture Behavioral of counter_buttons is
    
    signal cnt : integer range 0 to 99 := 0;
    signal previous_increase : std_logic;
    signal previous_decrease : std_logic;
begin

    output <= std_logic_vector(to_unsigned(cnt, 7));

process(clk)

begin
    if (clk'event and clk = '1') then
        if (previous_increase = '0' and increase = '1') then
            if cnt >= 99 then
                cnt <= 99;
            elsif cnt < 0 then
                cnt <= 0;
            else 
                cnt <= cnt +1;       
            end if;
        elsif (previous_decrease = '0' and decrease = '1') then
            if cnt <= 0 then
                cnt <= 0;
            elsif cnt > 99 then
                cnt <= 99;
            else
                cnt <= cnt - 1;     
            end if;
        end if;
        previous_increase <= increase;
        previous_decrease <= decrease;
    end if;
    
end process;
    

end Behavioral;
