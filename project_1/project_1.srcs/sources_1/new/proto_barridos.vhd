

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity proto_barridos is
Port (
    clk       : in std_logic;
    decoded_5 : in std_logic_vector(6  downto 0);
    decoded_4 : in std_logic_vector(6  downto 0);
    decoded_3 : in std_logic_vector(6  downto 0);
    decoded_2 : in std_logic_vector(6  downto 0);
    decoded_1 : in std_logic_vector(6  downto 0);
    decoded_0 : in std_logic_vector(6  downto 0);
    to_disp   : out std_logic_vector(6 downto 0);
    chipsel   : out std_logic_vector(5 downto 0) -- With this array we can select the display which is going to be activated. 
 );
end proto_barridos;

architecture Behavioral of proto_barridos is

    signal tmp     : std_logic_vector(5 downto 0);
        
begin

    chipsel <= tmp;

process(tmp)
begin
    case tmp is
         when "011111" => to_disp <= decoded_5;
         when "101111" => to_disp <= decoded_4;
         when "110111" => to_disp <= decoded_3;
         when "111011" => to_disp <= decoded_2;
         when "111101" => to_disp <= decoded_1;
         when "111110" => to_disp <= decoded_0;
         when others => to_disp <= decoded_5;
     end case;
end process;

process(clk, tmp)
    begin

    if (clk'event and clk = '1') then
        if (tmp = "011111") then
            tmp <= "101111";
         elsif tmp = "101111" then
            tmp <= "110111";
         elsif tmp = "110111" then
            tmp <= "111011";
         elsif tmp = "111011" then
            tmp <= "111101";
         elsif tmp = "111101" then
            tmp <= "111110";
         else
            tmp <= "011111";
        end if;
    end if;

end process;


end Behavioral;
