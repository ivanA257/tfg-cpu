----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.01.2021 16:20:12
-- Design Name: 
-- Module Name: data_sender - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package bus_multiplexer_pkg is
        type bus_array is array(natural range <>) of std_logic_vector(31 downto 0);
end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.bus_multiplexer_pkg.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;





entity data_sender is
    generic (
        max_count : integer := 3
    );
    Port (
        clk : in std_logic;
        reset : in std_logic;
        nonbusy : in std_logic;
        input : in bus_array(max_count - 1 downto 0);
        out_nonbusy : out std_logic := '0';
        out_begin   : out std_logic := '0';
        output      : out std_logic_vector(31 downto 0) := (others => '1');
        out_count : out std_logic_vector(1 downto 0);
        tester : out std_logic := '0'
     );
end data_sender;



architecture Behavioral of data_sender is

    signal count : integer range 0 to max_count+1 := max_count+1;
    type state_type is (s_nonbusy, zero, one, two, three);
    signal state : state_type := s_nonbusy;
    signal inner_out_begin : std_logic;
    signal pre_output : std_logic_vector(31 downto 0);
    

    

begin

    out_count <= std_logic_vector(to_unsigned(count, 2));
    out_begin <= inner_out_begin;
    
    with count select output <=
        x"0000000A" when max_count,
        pre_output  when others;
        
    process(count)
    begin
        for i in 0 to max_count-1 loop
            if i = count then
                pre_output <= input(i);
            end if;
        end loop;
    end process;

    -- Counting process
    process(clk, nonbusy, reset, count)
    begin
        if rising_edge(clk) then
            inner_out_begin <= '0';
            if nonbusy = '1' then
                if count = max_count+1 then
                    out_nonbusy <= '1';
                    inner_out_begin <= '0';
                    count <= max_count+1;
                    if reset = '1' then
                        inner_out_begin <= '1';
                        tester <= '1';
                        count <= 0;
                        out_nonbusy <= '0';
                    end if;
                elsif count = max_count then
                    count <= count + 1;
                    inner_out_begin <= '0';
                else
                    count <= count + 1;
                    inner_out_begin <= '1';
                end if;
            end if;
        end if;
    end process;


end Behavioral;
