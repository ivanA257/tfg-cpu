library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity control_unit is
    generic (
        N : integer := 16
    );
    port (
    clk : in std_logic;
    zr : in std_logic;
    ng : in std_logic;
    instruction : in std_logic_vector (N-1 downto 0);
    w_load : out std_logic_vector (15 downto 0);
    w_enable : out std_logic_vector (15 downto 0);
    alu : out std_logic_vector (5 downto 0);
    alu_mul : out std_logic;
    pc_mul : out std_logic;
    m_load : out std_logic;
    to_bus : out std_logic_vector (N-1 downto 0)
    );
end entity control_unit;

architecture behav of control_unit is

    signal decoded_load : std_logic_vector (15 downto 0);
    signal decoded_enable : std_logic_vector (15 downto 0);
    signal nowhere : std_logic;
    signal jump_out : std_logic;
    signal zero : std_logic_vector (N-16 downto 0) := (others => '0');
    constant disp : integer := 4;

    component N_decoder is
        generic (
            M : integer :=2
        );
        Port ( 
            input : in std_logic_vector(M-1 downto 0);
            output : out std_logic_vector((2**M)-1 downto 0)
        );
    end component;
    
    component jumper is
        Port (
            j1 : in std_logic;
            j2 : in std_logic;
            j3 : in std_logic;
            zr : in std_logic;
            ng : in std_logic;
            output : out std_logic
    );
    end component;
begin

    -- Jumper addition
    jumps: jumper 
        port map(
            j1 => instruction(disp - 2),
            j2 => instruction(disp - 3),
            j3 => instruction(disp - 4),
            zr => zr,
            ng => ng,
            output => jump_out
        );

    load_decoder: N_decoder generic map(M=>4)
        port map(
            input => instruction(disp + 9 downto disp + 6),
            output => decoded_load
                );

    enable_decoder: N_decoder generic map(M=>4)
        port map(
            input => instruction(disp + 13 downto disp + 10),
            output => decoded_enable
                );
                
    alu_mul <= instruction(disp + 15);
    to_bus <= zero & instruction(disp + 14 downto disp + 0);

    process (clk)
    begin
        if rising_edge (clk) then
            if instruction(disp + 15) = '1' then
                -- Todo lo demas a 0
                w_load <= (others => '0');
                w_enable <= (others => '0');
                alu <= (others => '0');
                pc_mul <= '0';
                m_load <= '0';
                -- Y to_bus con los bits que marque la instrucción. Ahora que lo pienso, esto puede pasar siempre, sin ser condicional.
            else
                alu <= instruction(disp + 4 downto disp - 1);
                m_load <= instruction(disp + 5);
                pc_mul <= jump_out;
                if instruction(disp + 14) = '0' then -- Caso de solo enable
                    w_load <= (others => '0'); -- Ponemos load a 0
                    w_enable <= decoded_enable;
                else -- Caso load + enable
                    w_load <= decoded_load;
                    w_enable <= decoded_enable;
                end if;
            end if;
        end if;
    end process;



end behav;

 
