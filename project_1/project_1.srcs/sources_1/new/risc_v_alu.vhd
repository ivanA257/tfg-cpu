library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity risc_v_alu is
    generic (
        N : integer := 32 
    );
    port (
        funct3 : in std_logic_vector (2 downto 0);
        in_x : in std_logic_vector (N-1 downto 0);
        in_y : in std_logic_vector (N-1 downto 0);
        add_sub : in std_logic;
        output : out std_logic_vector (N-1 downto 0)
    );
end entity risc_v_alu;

architecture behav of risc_v_alu is

    component N_bit_adder_organic is
        generic (
            N : integer := 4
        );
        port (
            input_a : in std_logic_vector (N-1 downto 0);
            input_b : in std_logic_vector (N-1 downto 0);
            output  : out std_logic_vector (N-1 downto 0);
            o_carry : out std_logic
        );
    end component;
     
    signal pre_sum_a : std_logic_vector(N-1 downto 0);
    signal sum : std_logic_vector(N-1 downto 0);
    signal sum_final : std_logic_vector(N-1 downto 0);
    signal zeros_31 : std_logic_vector(30 downto 0) := (others => '0');
    signal secondshift : std_logic_vector(N-1 downto 0);
    signal compared_s : std_logic_vector(N-1 downto 0);
    signal compared_u : std_logic_vector(N-1 downto 0);

begin

    adder: N_bit_adder_organic generic map(N=>N)
        port map(
            input_a => pre_sum_a,
            input_b => in_y,
            output => sum
            );

    with add_sub select pre_sum_a <= 
        not in_x when '1',
        in_x     when others;

    with add_sub select sum_final <= 
        not sum when '1',
        sum     when others;
    
    
    with add_sub select secondshift <=
        std_logic_vector(unsigned(in_x) srl to_integer(unsigned(in_y))) when '0',
        To_StdLogicVector(to_bitvector(in_x) sra to_integer(unsigned(in_y))) when '1',
        (others => 'Z') when others;
        
    
    
    with funct3 select output <=
        sum_final when "000",
        std_logic_vector(unsigned(in_x) sll to_integer(unsigned(in_y))) when "001",
        secondshift when "101",
        compared_s  when "010",
        compared_u  when "011",
        in_x xor in_y when "100",
        in_x or in_y  when "110",
        in_x and in_y when "111",     
        (others => 'Z') when others;
        
    process(in_x, in_y)
    begin
        if signed(in_x) < signed(in_y) then
            compared_s <= zeros_31 & '1';
        else
            compared_s <= zeros_31 & '0';
        end if;
        if unsigned(in_x) < unsigned(in_y) then
            compared_u <= zeros_31 & '1';
        else
            compared_u <= zeros_31 & '0';
        end if;
    end process;
    
--    process(funct3)
--    begin
--        if funct3 = "000" then -- Sum or subtraction
--            output <= sum_final;

----        elsif funct3 = "001" or funct3 = "101" then -- Shift
----        -- For a shift to correctly be performed we take rs1 in in_x, then we take
----        -- rs2 in in_y. Then output <= in_x << in_y. funct3 controls which shift to apply
----            if funct3(2) = '0' then
----                output <= std_logic_vector(unsigned(in_x) sll to_integer(unsigned(in_y)));
----            else
----                if add_sub = '0' then
----                    output <= std_logic_vector(unsigned(in_x) srl to_integer(unsigned(in_y)));
----                else
----                    output <=  To_StdLogicVector(to_bitvector(in_x) sra to_integer(unsigned(in_y)));
----                end if;
----            end if;

----        elsif funct3 = "010" or funct3 = "011" then -- Conditional set
----            if funct3(0) = '0' then
----                if signed(in_x) < signed(in_y) then
----                    output <= zeros_31 & '1';
----                else
----                    output <= zeros_31 & '0';
----                end if;
----            else
----                if unsigned(in_x) < unsigned(in_y) then
----                    output <= zeros_31 & '1';
----                else
----                    output <= zeros_31 & '0';
----                end if;
----            end if;

----        elsif funct3 = "100" then
----            output <= in_x xor in_y;

----        elsif funct3 = "110" then
----            output <= in_x or in_y;

----        elsif funct3 = "111" then
----            output <= in_x and in_y;

--        else
--            output <= (others => 'Z');
--        end if;
--    end process;

end behav;

