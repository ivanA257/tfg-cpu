----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2021 15:12:05
-- Design Name: 
-- Module Name: basic_uart - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity basic_uart is
  generic (
    counter_max : integer := 10417     -- Needs to be set correctly
    );
Port (
    i_clk : in std_logic;
    begin_tr : in std_logic;
    input : in std_logic_vector(7 downto 0);
    output: out std_logic := '1';
    out_nonbusy : out std_logic := '0'
 ); 
end basic_uart;

architecture Behavioral of basic_uart is
    signal counter : integer := 0;
    signal nice_clk : std_logic;
    type state_type is (nonbusy, start, zero, one, two, three, four, five, six, seven, stop);
    signal state : state_type := nonbusy;
    type button_type is (A, B, C);
    signal button_state : button_type;
    signal saved_input : std_logic_vector(7 downto 0);
    
begin

--process(i_clk)
--begin
--    if rising_edge(i_clk) then
--        if counter = counter_max then
--            counter <= 0;
--            nice_clk <= '1';
--        else
--            counter <= counter + 1;
--            nice_clk <= '0';
--        end if;
--    end if;
--end process;
nice_clk <= i_clk;

-- Data transmission cycle

process(nice_clk, button_state, begin_tr)
begin
    if rising_edge(nice_clk) then
        if state = nonbusy then
            out_nonbusy <= '1';
            if begin_tr = '1' then
--                if button_state = A then -- This can be uncomented when using a button to enable transmission.
                    state <= start;
                    out_nonbusy <= '0';
--                    button_state <= B;
                    output <= '0';
                    saved_input <= input;
--                elsif button_state = B then
--                    button_state <= C;
--                else
--                    button_state <= C;
--                end if;
--            else
--                button_state <= A;
            end if;
        elsif state = start then
            state <= zero;
            out_nonbusy <= '0';
            output <= saved_input(0);
        elsif state = zero then
            state <= one;
            out_nonbusy <= '0';
            output <= saved_input(1);
        elsif state = one then
            state <= two;
            out_nonbusy <= '0';
            output <= saved_input(2);
        elsif state = two then
            state <= three;
            out_nonbusy <= '0';
            output <= saved_input(3);
        elsif state = three then
            state <= four;
            out_nonbusy <= '0';
            output <= saved_input(4);
        elsif state = four then
            state <= five;
            out_nonbusy <= '0';
            output <= saved_input(5);
        elsif state = five then
            state <= six;
            out_nonbusy <= '0';
            output <= saved_input(6);
        elsif state = six then
            state <= seven;
            out_nonbusy <= '0';
            output <= saved_input(7);
        elsif state = seven then
            state <= stop;
            out_nonbusy <= '0';
            output <= '1';
        else
            state <= nonbusy;
            out_nonbusy <= '1';
        end if;
    end if;
end process;




end Behavioral;
