----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.01.2021 17:27:42
-- Design Name: 
-- Module Name: Byter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Byter is
    generic (
        max_count : integer range 0 to 5 := 5
    );
    Port (
        clk : in std_logic;
        reset : in std_logic;
        nonbusy : in std_logic;
        input : in std_logic_vector(31 downto 0);
        out_nonbusy : out std_logic := '0';
        out_begin   : out std_logic := '0';
        output      : out std_logic_vector(7 downto 0);
        out_count : out std_logic_vector(1 downto 0);
        tester : out std_logic := '0'
     );
end Byter;

architecture Behavioral of Byter is

    signal count : integer range 0 to max_count := max_count;
    type state_type is (s_nonbusy, zero, one, two, three, space);
    signal state : state_type := s_nonbusy;
    signal inner_out_nonbusy : std_logic;
    signal reset2 : std_logic;
    
    

begin

    out_count <= (others => '0');
    out_nonbusy <= inner_out_nonbusy;

    with state select output <=
        input(31 downto 24) when zero,
        input(23 downto 16) when one,
        input(15 downto  8) when two,
        input( 7 downto  0) when three,
        x"20"               when space,
        (others => '1')     when others;

    -- Counting process
    process(reset, nonbusy)
    begin
        if reset = '1' then
                    tester <= '1';
                    state <= zero;
                    inner_out_nonbusy <= '0';
                    out_begin <= '1';
            elsif (nonbusy'event and nonbusy = '1') then
                if state = s_nonbusy then
                    inner_out_nonbusy <= '1';
                    out_begin <= '0';
                    state <= s_nonbusy;
                    
                elsif state = zero then
                    state <= one;
                elsif state = one then
                    state <= two;
                elsif state = two then
                    state <= three;
    --                    out_begin <= '0';
                elsif state = three then
                    state <= space;
                    
                elsif state = space then
                    state <= s_nonbusy;
                    out_begin <= '0';
                    inner_out_nonbusy <= '1';
                end if;
           end if;
    end process;



end Behavioral;
