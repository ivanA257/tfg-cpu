library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity N_bit_adder_organic is
    generic (
        N : integer := 4
    );
    port (
        input_a : in std_logic_vector (N-1 downto 0);
        input_b : in std_logic_vector (N-1 downto 0);
        output  : out std_logic_vector (N-1 downto 0);
        o_carry : out std_logic
    );
end entity N_bit_adder_organic;

architecture behav of N_bit_adder_organic is
    component simple_adder is
        port (
            input_a      : in std_logic; 
            input_b      : in std_logic;
            input_carry  : in std_logic;
            output       : out std_logic;
            output_carry : out std_logic
        );
    end component;

    signal carry_v : std_logic_vector(N-1 downto 0);

begin

    generador: for i in 0 to N-1 generate
        first_bit: if i=0 generate
            adder0: simple_adder port map(
                input_a => input_a(i),
                input_b => input_b(i),
                input_carry => '0',
                output  => output(i),
                output_carry => carry_v(i)
                                         );
        end generate first_bit;
        i_bit: if i>0 generate
            adderX: simple_adder port map (
                input_a => input_a(i),
                input_b => input_b(i),
                input_carry => carry_v(i-1),
                output  => output(i),
                output_carry => carry_v(i)
                                      );
        end generate i_bit;
    end generate generador;   
        o_carry <= carry_v(N-1);

end behav;
