library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity alu_shifter is
    generic (
        N : integer := 4
    );
    port (
        input : in std_logic_vector (N-1 downto 0);
        ctr : in std_logic_vector (2 downto 0);
        output : out std_logic_vector (N-1 downto 0);
        o_c : out std_logic
    );

end entity alu_shifter;

architecture behav of alu_shifter is

    signal logic_left : std_logic_vector (N-1 downto 0);
    signal logic_right : std_logic_vector (N-1 downto 0);
    signal arith_left : std_logic_vector (N-1 downto 0);
    signal arith_right : std_logic_vector (N-1 downto 0);

begin
    process (input)
    begin
        for i in 1 to N-1 loop
            logic_left(i) <= input(i-1);
            arith_left(i) <= input(i-1);
        end loop;
        for j in 0 to N-2 loop
            logic_left(j) <= input(j+1);
            arith_left(j) <= input(j+1);
        end loop;
        
    end process;
    logic_left(0) <= '0';
    logic_right(N-1) <= '0';
    arith_left(0) <= input(N-1);
    arith_right(N-1) <= input(0);

    with ctr select output <=
        logic_left  when "010",
        logic_right when "001",
        arith_left  when "110",
        arith_right when "101",
        input       when others;

    with ctr select o_c <=
        input(N-1)  when "010",
        input(0)    when "001",
        input(N-1)  when "110", -- Here I'm not sure if it makes any sense to carry out this bit shen the operation is just a roll.
        input(0)    when "101",
        '0'         when others;
        

end behav;

