library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pc_risc_v is
generic(
    N : integer := 16
);
Port (
    clk : in std_logic;
    rst : in std_logic;
    inc : in std_logic;
    input : in std_logic_vector(N-1 downto 0);
    output : out std_logic_vector(N-1 downto 0);
    incremented: out std_logic_vector (N-1 downto 0)
 );
end pc_risc_v;

architecture Behavioral of pc_risc_v is
    signal value : unsigned(N-1 downto 0);
begin

    output <= std_logic_vector(value);
    incremented <= std_logic_vector(value+4);
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                value <= (others => '0');
            else
                if inc = '0' then
                    value <= value + 4;
                else 
                    value <= unsigned(input);
                end if;
            end if;
        end if;
    end process;

end Behavioral;
