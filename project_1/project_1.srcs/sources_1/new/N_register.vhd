library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity n_reg is
    generic (
        N : integer := 4
            );
    port (
        d : in std_logic_vector (N-1 downto 0);      
        clk : in std_logic;
        chipsel : in std_logic;
        q : out std_logic_vector (N-1 downto 0)
    );
end entity n_reg;

architecture behav of n_reg is

    

begin
    
    reg: for i in 0 to N-1 generate
        process (clk)
        begin
        
            if rising_edge (clk) then
                if chipsel = '1' then
                    q(i) <= d(i);        
                end if;
            end if;
        end process;
    end generate reg;

end behav;
    
