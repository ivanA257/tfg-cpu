
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity multiplexer is
Port ( 
    input1 : in std_logic;
    input2 : in std_logic;
    input3 : in std_logic;
    input4 : in std_logic;
    selec  : in std_logic_vector(1 downto 0);
    output : out std_logic
);
end multiplexer;

architecture Behavioral of multiplexer is

begin

process (input1, input2, selec)
begin
    case selec is
        when "00" => output <= input1;
        when "01" => output <= input2;
        when "10" => output <= input3;
        when "11" => output <= input4;
        when others => output <= input1;
    end case;
    
end process;

end Behavioral;
