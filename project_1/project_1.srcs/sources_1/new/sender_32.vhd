----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.01.2021 18:30:55
-- Design Name: 
-- Module Name: sender_32 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package bus_multiplexer_pkg is
        type bus_array is array(natural range <>) of std_logic_vector(31 downto 0);
end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.bus_multiplexer_pkg.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sender_32 is
generic (
    word_amount : integer := 4
);
Port (
    reset : in std_logic;
    clk : in std_logic;
    input : in bus_array(word_amount - 1 downto 0);
    output : out std_logic;
    out_sender_nonbusy : out std_logic;
    out_byter_nonbusy : out std_logic;
    uart_nonbusy : out std_logic;
    counter     : out std_logic_vector(1 downto 0);
    button_pressed : out std_logic
 );
end sender_32;

architecture Behavioral of sender_32 is

    component Byter is
    generic (
        max_count : integer range 0 to 5 := 5
    );
    Port (
        clk : in std_logic;
        reset : in std_logic;
        nonbusy : in std_logic;
        input : in std_logic_vector(31 downto 0);
        out_nonbusy : out std_logic;
        out_begin   : out std_logic;
        output      : out std_logic_vector(7 downto 0);
        out_count : out std_logic_vector(1 downto 0);
        tester : out std_logic
     );
    end component;
    
    component basic_uart is 
          generic (
        counter_max : integer := 10417     -- Needs to be set correctly
        );
    Port (
        i_clk : in std_logic;
        begin_tr : in std_logic;
        input : in std_logic_vector(7 downto 0);
        output: out std_logic := '1';
        out_nonbusy : out std_logic
     ); 
    end component;
    
    component data_sender is
        generic (
        max_count : integer := word_amount
    );
    Port (
        clk : in std_logic;
        reset : in std_logic;
        nonbusy : in std_logic;
        input : in bus_array(max_count - 1 downto 0);
        out_nonbusy : out std_logic := '0';
        out_begin   : out std_logic := '0';
        output      : out std_logic_vector(31 downto 0);
        out_count : out std_logic_vector(1 downto 0);
        tester : out std_logic := '0'
     );
    end component;
    
    signal b_nonbusy : std_logic;
    signal b_begin : std_logic;
    signal b_output : std_logic_vector(7 downto 0);
    type button_type is (A, B, C);
    signal button_state : button_type := A;
    signal go_reset : std_logic;
    signal hola_tester : std_logic;
    signal nice_clk : std_logic;
    signal ctr : integer := 0;
    signal byter_nonbusy : std_logic;
    signal sender_begin : std_logic;
    signal sender_output : std_logic_vector(31 downto 0);
   
begin

process(clk)
begin
    if rising_edge(clk) then
        if ctr = 868      then
            ctr <= 0;
            nice_clk <= '1';
        else
            ctr <= ctr + 1;
            nice_clk <= '0';
        end if;
    end if;
end process;



    process(nice_clk)
    begin
        if rising_edge(nice_clk) then
            if reset = '1' then
                if button_state = A then
                    button_state <= B;
                elsif button_state = B then
                    go_reset <= '1';
                    button_state <= C;
                elsif button_state = C then
                    button_state <= C;
                    go_reset <= '0';
                else
                    button_state <= A;
                    go_reset <= '0';
                end if;
            else
                button_state <= A;
                go_reset <= '0';
            end if;
        end if;
    end process;

    uart_nonbusy <= b_nonbusy;
    out_byter_nonbusy <= byter_nonbusy;
    button_pressed <= sender_begin;
    inst_byter: Byter generic map(max_count => 5)
        port map (
            clk => nice_clk,
            reset => sender_begin,
            nonbusy => b_nonbusy,
            input => sender_output,
            out_begin => b_begin,
            out_nonbusy => byter_nonbusy,
            output => b_output,
            tester => hola_tester
            );
            
    inst_uart: basic_uart generic map (counter_max => 868)
        port map(
            i_clk => nice_clk,
            begin_tr => b_begin,
            input => b_output,
            output => output,
            out_nonbusy => b_nonbusy
        );
        
    inst_data_sender: data_sender generic map (max_count => word_amount)
        port map (
            clk => nice_clk,
            reset => go_reset,
            nonbusy => byter_nonbusy,
            input => input,
            out_nonbusy => out_sender_nonbusy,
            out_begin => sender_begin,
            out_count => counter,            
            output => sender_output
        );


end Behavioral;
