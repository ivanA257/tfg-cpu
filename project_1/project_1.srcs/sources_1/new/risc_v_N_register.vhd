library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity risc_v_n_reg is
    generic (
        N : integer := 4
            );
    port (
        d_a : in std_logic_vector (N-1 downto 0);      
        d_b : in std_logic_vector (N-1 downto 0);      
        d_r : in std_logic_vector (N-1 downto 0);      
        clk : in std_logic;
        load_a : in std_logic;
        load_b : in std_logic;
        load_r : in std_logic;
        q : out std_logic_vector (N-1 downto 0) := (others => '0')
    );
end entity risc_v_n_reg;

architecture behav of risc_v_n_reg is

    signal load : std_logic;
    signal loads : std_logic_vector(2 downto 0);
    signal d : std_logic_vector (N-1 downto 0) := (others => '0');   
    
begin

    loads <= load_a & load_b & load_r;
    load <= load_a or load_b or load_r;

    with loads select d <=
        d_a when "100",
        d_b when "010",
        d_r when others;
       -- (others => 'Z') when others;

    reg: for i in 0 to N-1 generate
        process (clk)
        begin
        
            if rising_edge (clk) then
                if load = '1' then
                    q(i) <= d(i);             
                end if;
            end if;
        end process;
    end generate reg;


    
--    reg: for i in 0 to N-1 generate
--        process (clk)
--        begin
        
--            if rising_edge (clk) then
--                if load_a = '1' then
--                    q(i) <= d_a(i);        
--                elsif load_b = '1' then
--                    q(i) <= d_b(i);        
--                elsif load_r = '1' then
--                    q(i) <= d_r(i);        
--                end if;
--            end if;
--        end process;
--    end generate reg;

end behav;
    
