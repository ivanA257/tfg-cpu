library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity risc_v_N_decoder is
generic (
    M : integer :=2
);
Port ( 
    input : in std_logic_vector(M-1 downto 0);
    enable : in std_logic;
    output : out std_logic_vector((2**M)-1 downto 0) := (others => '0')
);
end risc_v_N_decoder;

architecture Behavioral of risc_v_N_decoder is

begin

--    output(conv_integer(input)) <= enable;

    process(input, enable)
    
    begin
        for i in 0 to (2**M)-1 loop
            if unsigned(input) = i and enable = '1' then
                output(i) <= '1';
            else
                output(i) <= '0'; 
            end if;
        end loop;
    end process;


end Behavioral;
