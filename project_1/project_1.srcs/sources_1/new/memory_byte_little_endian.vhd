-- Block RAM with Resettable Data Output
-- File: rams_sp_rf_rst.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

entity native_memory_ble is
  generic(
    N : integer := 32; -- Word size
    M : integer := 32; -- Address bus width
    K : integer := 10  -- log_2(Memory size)
  );
  port(
    clk  : in  std_logic;
    we   : in  std_logic;
    addr : in  std_logic_vector(M-1 downto 0);
    pcin : in  std_logic_vector(M-1 downto 0);
    di   : in  std_logic_vector(N-1 downto 0);
    width : in  std_logic_vector(1 downto 0);
    unsig : in std_logic;
    do   : out std_logic_vector(N-1 downto 0);
    inst : out std_logic_vector(N-1 downto 0)
  );
end native_memory_ble;

architecture syn of native_memory_ble is
  type ram_type is array ((2**K)-1 downto 0) of std_logic_vector(N-1 downto 0);
    signal ram : ram_type := ( 0 => x"00000317",
                               1 => x"00830067",
                               2 => x"fe010113",
                               3 => x"00112e23",
                               4 => x"00812c23",
                               5 => x"02010413",
                               6 => x"06400793",
                               7 => x"fef42623",
                               8 => x"00300793",
                               9 => x"fef42423",
                               10 => x"fe842583",
                               11 => x"fec42503",
                               12 => x"00050613",
                               13 => x"00000513",
                               14 => x"0015f693",
                               15 => x"00068463",
                               16 => x"00c50533",
                               17 => x"0015d593",
                               18 => x"00161613",
                               19 => x"fe0596e3",
                               20 => x"00050793",
                               21 => x"fef42223",
                               22 => x"00000793",
                               23 => x"00078513",
                               24 => x"01c12083",
                               25 => x"01812403",
                               26 => x"02010113",
                               27 => x"00008067",
-- This is the program for the sum (test1)  
--    signal ram : ram_type := ( 0 => x"00000317",
--                               1 => x"00830067",
--                               2 => x"fe010113",
--                               3 => x"00812e23",
--                               4 => x"02010413",
--                               5 => x"06400793",
--                               6 => x"fef42623",
--                               7 => x"00300793",
--                               8 => x"fef42423",
--                               9 => x"fec42703",
--                              10 => x"fe842783",
--                              11 => x"00f707b3",
--                              12 => x"fef42223",
--                              13 => x"00000793",
--                              14 => x"00078513",
--                              15 => x"01c12403",
--                              16 => x"02010113",
--                              17 => x"00008067",
                            others => (others => '0'));
    
    signal read_mem : std_logic_vector(N-1 downto 0);
    signal pre_do : std_logic_vector(N-1 downto 0);
    signal casi_do : std_logic_vector(N-1 downto 0);
    signal masker : std_logic_vector(3 downto 0);
    signal mask : std_logic_vector(3 downto 0);
    signal padding : std_logic_vector(N-1 downto 0);
    signal post_di : std_logic_vector(N-1 downto 0);
    signal real_address : std_logic_vector(11 downto 2);
    
    begin
    
    real_address <= addr(11 downto 2);  -- Aqui cogemos los de abajo para que no llame a una direcci�n m�s alta y pete.
    
    inst <= ram(conv_integer(pcin(11 downto 2)));
    
    read_mem <= ram(conv_integer(real_address));
    
    -- Reading alignment
    with addr(1 downto 0) select pre_do <= 
        read_mem                          when "00",
        x"00" & read_mem(31 downto 8)     when "01",
        x"0000" & read_mem(31 downto 16)  when "10",
        x"000000" & read_mem(31 downto 24) when "11",
        read_mem                          when others;
    
    
    -- Padding
    
    process(unsig, width, pre_do)
    begin
        padding <= (others => '0');
        if unsig = '0' then
            case width is
                when "00" => -- Single byte case
                  if pre_do(7) = '1' then
                     padding <= (others => '1');
                  end if;
                when others => -- Half word case
                  if pre_do(15) = '1' then
                     padding <= (others => '1');
                  end if;              
            end case;
        end if;
    end process;
    
    with width select casi_do <=
        padding(31 downto 8) & pre_do(7 downto 0)   when "00",
        padding(31 downto 16) & pre_do(15 downto 0) when "01",
        pre_do                                      when others;
        
        
    masker <= width & addr(1 downto 0);

      -- Writing mask
    with masker select mask <=
        "0001" when "0000",
        "0010" when "0001",
        "0100" when "0010",
        "1000" when "0011",
        
        "0011" when "0100",
        "0110" when "0101",
        "1100" when "0110",
        "1000" when "0111",
        
        "1111" when "1000",
        "1110" when "1001",
        "1100" when "1010",
        "1000" when "1011",
        
        "0000" when others;      
        
    -- Writing alignment
    with addr(1 downto 0) select post_di <=
        di when "00", 
        di(23 downto 0) & x"00"     when "01",
        di(15 downto 0) & x"0000"   when "10",
        di( 7 downto 0) & x"000000" when others;
        
  
    process(clk)
    begin
    if clk'event and clk = '1' then
        if we = '1' then        -- write enable
            if mask(0) = '1' then 
                ram(conv_integer(real_address))( 7 downto  0) <= post_di(7 downto 0);
            end if;
            if mask(1) = '1' then 
                ram(conv_integer(real_address))(15 downto  8) <= post_di(15 downto 8);
            end if;
            if mask(2) = '1' then 
                ram(conv_integer(real_address))(23 downto 16) <= post_di(23 downto 16);
            end if;
            if mask(3) = '1' then 
                ram(conv_integer(real_address))(31 downto 24) <= post_di(31 downto 24);
            end if;
        end if;
    end if;
    if clk'event and clk = '0' then
        do <= casi_do;
    end if;
    end process;
      
end syn;