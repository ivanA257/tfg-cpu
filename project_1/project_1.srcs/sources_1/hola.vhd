
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TwoStateMach is
Port ( 
    clk    : in std_logic;
    button : in std_logic;
    output : out std_logic
);
end TwoStateMach;

architecture Behavioral of TwoStateMach is
    
    signal tmp : std_logic;
    signal previous_button : std_logic := '0';
begin

    output <= tmp;

process(button)
begin
    if (clk'event and clk = '1') then
        if (previous_button = '0' and button = '1') then
            if tmp = '1' then
                tmp <= '0';
            elsif tmp = '0' then
                tmp <= '1';
            else 
                tmp <= '0';
            end if;
        end if;
    previous_button <= button;
    end if;
end process;

entity hola is
	generic (
		N := 
	);
	port (
		
	);
end entity hola;

if  then
    		
end if;
end Behavioral;
