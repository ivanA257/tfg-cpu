----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.11.2020 19:23:38
-- Design Name: 
-- Module Name: multiplier_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity multiplier_sim is
--  Port ( );
end multiplier_sim;

architecture Behavioral of multiplier_sim is
    
    component proto_multiplier is
    generic(
    N : integer := 7);  -- I'm using seven bits because I need numbers up to 99.
Port ( 
    input1 : in  std_logic_vector(N-1 downto 0);
    input2 : in  std_logic_vector(N-1 downto 0);
    mult   : out std_logic_vector(N-1 downto 0)
);
    end component;
    
    signal input1 : std_logic_vector(6 downto 0);
    signal input2 : std_logic_vector(6 downto 0);
    signal mult : std_logic_vector(6 downto 0);
begin

    uut: proto_multiplier port map(input1 => input1, input2 => input2, mult => mult);
    
    input1 <= "0000010";
    input2 <= "0000011";


end Behavioral;
