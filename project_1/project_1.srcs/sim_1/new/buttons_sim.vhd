library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity buttons_sim is
--Port ( );
end buttons_sim;

architecture Behavioral of buttons_sim is
    constant T : time := 50 ns;
    
    component counter_buttons is
    Port (
        clk      : in std_logic;
        increase : in std_logic;
        decrease : in std_logic;
        output   : out std_logic_vector(6 downto 0)
    );
    end component;
    
    signal clk : std_logic;
    signal increase : std_logic;
    signal decrease : std_logic;
    signal output : std_logic_vector(6 downto 0);
    
begin
    
    uut: counter_buttons port map(clk => clk, increase => increase,
                                  decrease => decrease, output => output);
    
    increase <= '0', '1' after 200 ns, '0' after 300 ns, '1' after 400 ns;
  --  decrease <= '0', '1' after 400 ns;
    
    process
    begin
        clk <= '0';
        wait for T/2;
        clk <= '1';
        wait for T/2;
    end process;


end Behavioral;
