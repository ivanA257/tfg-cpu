-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 4.11.2020 22:16:54 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_simple_adder is
end tb_simple_adder;

architecture tb of tb_simple_adder is

    component simple_adder
        port (input_a      : in std_logic;
              input_b      : in std_logic;
              input_carry  : in std_logic;
              output       : out std_logic;
              output_carry : out std_logic);
    end component;

    signal input_a      : std_logic;
    signal input_b      : std_logic;
    signal input_carry  : std_logic;
    signal output       : std_logic;
    signal output_carry : std_logic;


begin

    uut : simple_adder
    port map (input_a      => input_a,
              input_b      => input_b,
              input_carry  => input_carry,
              output       => output,
              output_carry => output_carry);

    -- Clock generation

    --  EDIT: Replace YOURCLOCKSIGNAL below by the name of your clock as I haven't guessed it
    --  YOURCLOCKSIGNAL <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        input_a <= '0';
        input_b <= '0';
        input_carry <= '0';

        -- EDIT Add stimuli here
        wait for 100 ns;
        
        input_a <= '1';
        wait for 100 ns;
        input_b <= '1';
        wait for 100 ns;
        input_carry <= '1';
        

        -- Stop the clock and hence terminate the simulation
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_simple_adder of tb_simple_adder is
    for tb
    end for;
end cfg_tb_simple_adder;