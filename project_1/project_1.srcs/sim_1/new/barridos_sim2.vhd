----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.10.2020 15:26:11
-- Design Name: 
-- Module Name: barridos_sim2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity barridos_sim2 is
--  Port ( );
end barridos_sim2;

architecture Behavioral of barridos_sim2 is
    
    constant T : time := 50 ns;
component proto_barridos is
    Port (
    clk       : in std_logic;
    decoded_5 : in std_logic_vector(6  downto 0);
    decoded_4 : in std_logic_vector(6  downto 0);
    decoded_3 : in std_logic_vector(6  downto 0);
    decoded_2 : in std_logic_vector(6  downto 0);
    decoded_1 : in std_logic_vector(6  downto 0);
    decoded_0 : in std_logic_vector(6  downto 0);
    to_disp   : out std_logic_vector(6 downto 0);
    chipsel   : out std_logic_vector(5 downto 0) -- With this array we can select the display which is going to be activated. 
 );
end component;


    signal clk : std_logic;
    signal decoded_5 : std_logic_vector(6 downto 0);
    signal decoded_4 : std_logic_vector(6 downto 0);
    signal decoded_3 : std_logic_vector(6 downto 0);
    signal decoded_2 : std_logic_vector(6 downto 0);
    signal decoded_1 : std_logic_vector(6 downto 0);
    signal decoded_0 : std_logic_vector(6 downto 0);
    signal to_disp   : std_logic_vector(6 downto 0);
    signal chipsel   : std_logic_vector(5 downto 0);
    
    
    
begin

    decoded_5 <= "0100100";
    decoded_4 <= "1001100";
    decoded_3 <= "0000110";
    decoded_2 <= "0010010";
    decoded_1 <= "1001111";
    decoded_0 <= "0000001";
    
    uut: proto_barridos port map (clk       => clk, 
                                  decoded_5 => decoded_5,
                                  decoded_4 => decoded_4,
                                  decoded_3 => decoded_3,
                                  decoded_2 => decoded_2,
                                  decoded_1 => decoded_1,
                                  decoded_0 => decoded_0,  
                                  to_disp   => to_disp,
                                  chipsel   => chipsel);  

process 
    begin
        clk <= '0';
        wait for T/2;
        clk <= '1';
        wait for T/2;
    end process;

end Behavioral;
