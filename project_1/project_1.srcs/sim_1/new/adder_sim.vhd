----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.10.2020 12:47:25
-- Design Name: 
-- Module Name: adder_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adder_sim is
end adder_sim;

architecture Behavioral of adder_sim is

component adder_trial is
    port (
        i : in  std_logic_vector(2 downto 0);
        o : out std_logic_vector(1 downto 0)
    );
end component;
    signal i : std_logic_vector(2 downto 0);
    signal o : std_logic_vector(1 downto 0);
begin
    uut: adder_trial port map (
i => i, o => o);
     i <= "000", 
          "001" after 10 ns, 
          "010" after 20 ns, 
          "011" after 30 ns, 
          "100" after 40 ns, 
          "101" after 50 ns, 
          "110" after 60 ns, 
          "111" after 70 ns;                  
end Behavioral;



