----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.10.2020 11:23:36
-- Design Name: 
-- Module Name: hello_2_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hello_2_sim is
--  Port ( );
end hello_2_sim;

architecture Behavioral of hello_2_sim is
component hello_2 is 
port(
    input1 : in std_logic;
    input2 : in std_logic;
    output : out std_logic);
end component;

    signal input1 : std_logic;
    signal input2 : std_logic;
    signal output : std_logic;
begin

    uut: hello_2 port map( input1 => input1,
                           input2 => input2,
                           output => output);

    stimulus: process
    begin
    
        input1 <= '0';
        input2 <= '0';
        wait for 10 ns;
        
        input1 <= '1';
        input2 <= '0';
        wait for 10 ns;
        
        input1 <= '0';
        input2 <= '1';
        wait for 10 ns;
        
        input1 <= '1';
        input2 <= '1';
        wait for 10 ns;
        
        wait;
        end process;

end Behavioral;
