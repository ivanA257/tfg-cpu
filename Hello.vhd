library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity adder_trial is
    port (
        i : in  std_logic_vector(2 downto 0);
        o : out std_logic_vector(1 downto 0)
    );
end adder_trial;

architecture adder_arch of adder_trial is

signal sum : std_logic;
signal carry : std_logic;

begin

sum <= i(0) xor i(1) xor i(2);
carry <= (i(0) and i(1)) or (i(0) and i(2)) or (i(1) and i(2));

o(1) <= carry;
o(0) <= sum;

end adder_arch;