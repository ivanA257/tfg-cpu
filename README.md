# tfg-cpu

Welcome to my bachelor's thesis! 

I started this project seeking to increase my own knowledge about computer processors and their design. With the coming of the months, it evolved from a rahter simple 8 bit processor to a 16 bit one and finally ended up becoming a 32 bit processor with complete capabilities to perform arithmetic operations, load and save data from memory and perform conditional jumps. 

Once I finished the design and testing of that first CPU I also wanted to be able to compile and run simple C code into it. However, creating a compiler from scratch is a huge task, which was completely out of the scope of the project. 

In this situation, I decided that the best idea was to implement an existing ISA; specifically I chose RISC-V. The major reasons for that election was its good documentation and its modularity. The latter allowed me to only implement those parts I needed. In the end, as I only wanted to run simple programs, I only implemented the most basic module of RISC-V, RV32I.

I hope you find my work interesting <3. Any kind of feedback is alwasy appreciated!
