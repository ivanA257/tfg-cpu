onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_cpu/dut/clk
add wave -noupdate /tb_cpu/dut/instruction
add wave -noupdate /tb_cpu/dut/c_bus
add wave -noupdate /tb_cpu/dut/custom_q
add wave -noupdate /tb_cpu/dut/custom_enable
add wave -noupdate /tb_cpu/dut/mar_q
add wave -noupdate /tb_cpu/dut/mdr_d
add wave -noupdate /tb_cpu/dut/native_mem/di
add wave -noupdate /tb_cpu/dut/native_mem/do
add wave -noupdate /tb_cpu/dut/x_q
add wave -noupdate /tb_cpu/dut/y_q
add wave -noupdate /tb_cpu/dut/pc_r/output
add wave -noupdate /tb_cpu/dut/pc_r/rst
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {619 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 185
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {547 ns} {1025 ns}
