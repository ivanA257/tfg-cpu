onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_cpu/clk
add wave -noupdate -radix symbolic /tb_cpu/uut/control/instruction
add wave -noupdate /tb_cpu/uut/c_bus
add wave -noupdate /tb_cpu/instruction
add wave -noupdate /tb_cpu/uut/control/nowhere
add wave -noupdate /tb_cpu/uut/mar/d
add wave -noupdate /tb_cpu/uut/mar_load
add wave -noupdate /tb_cpu/uut/mar/chipsel
add wave -noupdate /tb_cpu/uut/mar/q
add wave -noupdate /tb_cpu/uut/control/nowhere
add wave -noupdate /tb_cpu/uut/mdr/d
add wave -noupdate /tb_cpu/uut/mdr_load
add wave -noupdate /tb_cpu/uut/mdr/chipsel
add wave -noupdate /tb_cpu/uut/mdr/q
add wave -noupdate /tb_cpu/uut/custom_enable
add wave -noupdate /tb_cpu/uut/custom/d
add wave -noupdate /tb_cpu/uut/custom/chipsel
add wave -noupdate /tb_cpu/uut/custom/q
add wave -noupdate /tb_cpu/uut/control/nowhere
add wave -noupdate -radix symbolic /tb_cpu/uut/control/w_load
add wave -noupdate -radix symbolic /tb_cpu/uut/control/w_enable
add wave -noupdate /tb_cpu/uut/control/alu_mul
add wave -noupdate /tb_cpu/uut/control/m_load
add wave -noupdate /tb_cpu/uut/control/to_bus
add wave -noupdate /tb_cpu/uut/control/nowhere
add wave -noupdate /tb_cpu/uut/m_load
add wave -noupdate /tb_cpu/uut/native_mem/addr
add wave -noupdate /tb_cpu/uut/native_mem/di
add wave -noupdate /tb_cpu/uut/native_mem/do
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {306 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 230
configure wave -valuecolwidth 38
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {314 ns}
