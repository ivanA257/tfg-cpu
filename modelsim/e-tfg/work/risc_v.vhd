library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity risc_v is
    generic(
        register_word_size : integer := 32;
        reg_ammount : integer := 32
           );
    port (
    clk : in std_logic;
    rst : in std_logic
    );
end entity risc_v;

architecture behav of risc_v is
    
    -- Immediate signals
    signal imm_u : std_logic_vector (register_word_size-1 downto 0);

    signal nowhere : std_logic;
    -- Bus
    signal c_bus_a : std_logic_vector (register_word_size-1 downto 0);
    signal c_bus_b : std_logic_vector (register_word_size-1 downto 0);
    signal r_bus : std_logic_vector (register_word_size-1 downto 0);
    
    -- Instruction
    signal instruction : std_logic_vector(register_word_size-1 downto 0);
    
    -- Register outputs
    signal mdr_q : std_logic_vector (register_word_size-1 downto 0);
    signal mar_q : std_logic_vector (register_word_size-1 downto 0);
    signal x_q : std_logic_vector (register_word_size-1 downto 0);
    signal zero_q : std_logic_vector (register_word_size-1 downto 0);
    signal custom_q : std_logic_vector (register_word_size-1 downto 0);
    type std_array is array (0 to (reg_ammount)-1) of std_logic_vector(register_word_size-1 downto 0);
    signal regs_q : std_array;
    signal jump_reg_q : std_logic_vector (register_word_size-1 downto 0);
    
    -- Register inputs
    signal mdr_d : std_logic_vector (register_word_size-1 downto 0);
    signal mdr_memory : std_logic_vector (register_word_size-1 downto 0);
    signal y_d : std_logic_vector (register_word_size-1 downto 0);
    
    -- Register loads
    signal mar_load : std_logic;
    signal mdr_load : std_logic;
    signal x_load : std_logic;
    signal regs_load_a : std_logic_vector ((reg_ammount)-1 downto 0);
    signal regs_load_b : std_logic_vector ((reg_ammount)-1 downto 0);
    signal regs_load_r : std_logic_vector ((reg_ammount)-1 downto 0);
    signal jump_reg_load : std_logic;

    
    -- Register enables
    signal mdr_enable : std_logic;
    signal zero_enable_a : std_logic;
    signal zero_enable_b : std_logic;
    signal pc_inc_enable : std_logic;
    signal enable_pc_inc_b : std_logic;
    signal regs_enable_a : std_logic_vector ((reg_ammount)-1 downto 0);
    signal regs_enable_b : std_logic_vector ((reg_ammount)-1 downto 0);

    -- Immediate enables
    signal enable_imm_u_a : std_logic;
    
    -- ALU related signals
    signal alu_add_sub : std_logic;
    signal alu_funct3 : std_logic_vector (2 downto 0);
    signal alu_b_mult : std_logic;
    signal alu_b_imm : std_logic_vector (register_word_size-1 downto 0);
    signal alu_zr : std_logic;
    signal alu_ng : std_logic;
    -- Control signals
    signal m_load : std_logic;
    signal m_save : std_logic;
    signal b_bus_multiplexer : std_logic_vector (register_word_size-1 downto 0);
    signal pc_mul : std_logic;
    signal jump : std_logic;
    signal branch : std_logic;
    signal branch_enable : std_logic;
    signal pcin : std_logic_vector (register_word_size-1 downto 0);
    signal mem_width : std_logic_vector(1 downto 0);
    signal mem_unsig : std_logic;
    
    -- Program counter
    signal pc_incremented : std_logic_vector (register_word_size-1 downto 0);
    signal pc_enable : std_logic;
    signal pc_input : std_logic_vector (register_word_size-1 downto 0);
    signal pc_pre_input : std_logic_vector (register_word_size-1 downto 0);
    signal pc_imm : std_logic_vector (register_word_size-1 downto 0);
    signal custom_pc : std_logic;
    
    -- Memory
    signal memory_read : std_logic_vector (register_word_size-1 downto 0);
    
    
    component n_reg is
        generic (
            N : integer := 4
                );
        port (
            d : in std_logic_vector (N-1 downto 0);      
            clk : in std_logic;
            chipsel : in std_logic;
            q : out std_logic_vector (N-1 downto 0)
        );
    end component;
    
    component pc_risc_v is
        generic(
            N : integer := 16
        );
        Port (
            clk : in std_logic;
            rst : in std_logic;
            inc : in std_logic;
            input : in std_logic_vector(N-1 downto 0);
            output : out std_logic_vector(N-1 downto 0);
            incremented: out std_logic_vector (N-1 downto 0)
         );
    end component;

    component N_M_RAM is
        generic (
            N : integer := 4; -- Word size
            M : integer := 2 -- Number of bits in the direction bus. The ammount of words in the memory is M**2
                );
        port (
            d : in std_logic_vector (N-1 downto 0);      
            clk : in std_logic;
            chipsel : in std_logic_vector(M-1 downto 0); -- This is the memory address
            load : in std_logic;
            q : out std_logic_vector (N-1 downto 0)
        );
    end component;
    
    component native_memory_ble is
        generic(
            N : integer := 32; -- Word size
            M : integer := 32; -- Address bus width
            K : integer := 10  -- log_2(Memory size)
            );
        port(
            clk  : in  std_logic;
            we   : in  std_logic;
            addr : in  std_logic_vector(M-1 downto 0);
            pcin : in  std_logic_vector(M-1 downto 0);
            di   : in  std_logic_vector(N-1 downto 0);
            width : in  std_logic_vector(1 downto 0);
            unsig : in std_logic;
            do   : out std_logic_vector(N-1 downto 0);
            inst : out std_logic_vector(N-1 downto 0)
        );
    end component;

    component risc_v_alu is
        generic (
            N : integer := 32 
        );
        port (
            funct3 : in std_logic_vector (2 downto 0);
            in_x : in std_logic_vector (N-1 downto 0);
            in_y : in std_logic_vector (N-1 downto 0);
            add_sub : in std_logic;
            output : out std_logic_vector (N-1 downto 0)
        );
    end component;
    

    component risc_v_decoder is
    generic (
        N : integer := 32 
    );
    port (
        clk : in std_logic;
        instruction : in std_logic_vector (N-1 downto 0);
        outer_imm_u : out std_logic_vector (N-1 downto 0);
        enable_imm_u_a : out std_logic;
        enable_pc_a : out std_logic;
        alu_mux : out std_logic;
        alu_funct3 : out std_logic_vector (2 downto 0);
        enable_pc_inc_a : out std_logic;
        pc_adder : out std_logic_vector (N-1 downto 0);
        jump : out std_logic; -- This one means inconditional jump.
        enable_md : out std_logic;
        load_md : out std_logic;
        funct7_significant : out std_logic;
        out_imm : out std_logic_vector (N-1 downto 0);
        jump_enable : out std_logic;
        w_enable_a : out std_logic_vector (31 downto 0);
        w_enable_b : out std_logic_vector (31 downto 0);
        w_load_a : out std_logic_vector (31 downto 0);
        w_load_b : out std_logic_vector (31 downto 0);
        w_load_r : out std_logic_vector (31 downto 0);
        mem_width : out std_logic_vector (1 downto 0);
        mem_unsig : out std_logic;
        custom_pc : out std_logic;
        enable_pc_inc_b : out std_logic
    );
    end component;
    

    component N_bit_adder_organic is
        generic (
            N : integer := 4
        );
        port (
            input_a : in std_logic_vector (N-1 downto 0);
            input_b : in std_logic_vector (N-1 downto 0);
            output  : out std_logic_vector (N-1 downto 0);
            o_carry : out std_logic
        );
    end component;

    component jumper_risc_v is
        generic( N : integer := 32);
        Port (
            funct3: in std_logic_vector (2 downto 0);
            a : in std_logic_vector (N-1 downto 0);
            b : in std_logic_vector (N-1 downto 0);
            branch_enable : in std_logic;
            output : out std_logic
        );
    end component;
    
    component risc_v_n_reg is
        generic (
        N : integer := 4
            );
    port (
        d_a : in std_logic_vector (N-1 downto 0);      
        d_b : in std_logic_vector (N-1 downto 0);      
        d_r : in std_logic_vector (N-1 downto 0);      
        clk : in std_logic;
        load_a : in std_logic;
        load_b : in std_logic;
        load_r : in std_logic;
        q : out std_logic_vector (N-1 downto 0) := (others => '0')
    );
    end component;
    
    
    
begin
    
    
    -- Both signals control if the pc jumps or not.
    pc_mul <= branch or jump;
    
    -- Multiplexer for the second input in the ALU
    b_bus_multiplexer <= alu_b_imm when (alu_b_mult = '1') else c_bus_b;

    -- Enables for the registers
    c_bus_a <= (others => '0') when (regs_enable_a(0) = '1') else (others => 'Z');
    reg_enabler_a: for j in 1 to (reg_ammount)-1 generate
        c_bus_a <= regs_q(j) when (regs_enable_a(j) = '1') else (others => 'Z');
    end generate reg_enabler_a;

    c_bus_a <= pc_incremented when (pc_inc_enable = '1') else (others => 'Z');
    c_bus_a <= imm_u when ( enable_imm_u_a = '1') else (others => 'Z');
    c_bus_a <= pcin when (pc_enable = '1') else (others => 'Z');

        -- Second bus
    c_bus_b <= (others => '0') when (regs_enable_b(0) = '1') else (others => 'Z');
    reg_enabler_b: for j in 1 to (reg_ammount)-1 generate
        c_bus_b <= regs_q(j) when (regs_enable_b(j) = '1') else (others => 'Z');
    end generate reg_enabler_b;
    c_bus_b <= pc_incremented when (enable_pc_inc_b = '1') else (others => 'Z');

    -- Enable for the memory read
    c_bus_b <= memory_read when (m_load = '1') else (others => 'Z');

    -- Enable for imm_u
    c_bus_a <= imm_u when (enable_imm_u_a = '1') else (others => 'Z');
    
    -- Enable fof pc_input
    pc_input <= r_bus when (custom_pc = '1') else pc_pre_input;



-- This is the memory implemented by me using latches, conceptually interesting because
-- one can see how the memory is built. However, it's extremely ineficient to sytethize.
-- Thus, it's better to use a built in memory module.

--    memory: N_M_RAM generic map(N => register_word_size, M => register_word_size)
--        port map(
--            clk => clk,
--            d => mdr_q,
--            chipsel => mar_q,
--            load => m_load,
--            q => mdr_memory
--                ); 
                
    native_mem: native_memory_ble generic map( N=> register_word_size, M => register_word_size)
        port map(
            clk => clk,
            we => m_save,
            addr => r_bus,
            pcin => pcin,
            di => c_bus_b,
            width => mem_width,
            unsig => mem_unsig,
            do => memory_read,
            inst => instruction
        );

    -- Implementing the program counter
    
    pc_r: pc_risc_v generic map(N => register_word_size)
        port map(
            clk => clk,
            rst => rst,
            inc => pc_mul,
            input => pc_input,
            output => pcin,
            incremented => pc_incremented
        );

    alu: risc_v_alu generic map(N => register_word_size)
        port map(
            funct3 => alu_funct3,
            in_x => c_bus_a,
            in_y => b_bus_multiplexer,
            add_sub => alu_add_sub,
            output => r_bus
            );

    jumps: jumper_risc_v generic map(N => register_word_size)
        port map(
            funct3 => alu_funct3,
            a => c_bus_a,
            b => b_bus_multiplexer,
            branch_enable => branch_enable,
            output => branch
            );

    decoder: risc_v_decoder generic map(N=>register_word_size)
        port map(
            clk => clk,
            instruction => instruction,
            outer_imm_u => imm_u,
            enable_imm_u_a => enable_imm_u_a,
            enable_pc_a => pc_enable,
            alu_mux => alu_b_mult,
            alu_funct3 => alu_funct3,
            enable_pc_inc_a => pc_inc_enable,
            pc_adder => pc_imm,
            jump => jump,
            enable_md => m_load,
            load_md => m_save,   -- We may have to delay this signal some way.
            funct7_significant => alu_add_sub,
            out_imm => alu_b_imm,
            jump_enable => branch_enable,
            w_enable_a => regs_enable_a,
            w_enable_b => regs_enable_b,
            w_load_a => regs_load_a,
            w_load_b => regs_load_b,
            w_load_r => regs_load_r,
            mem_width => mem_width,
            mem_unsig => mem_unsig,
            custom_pc => custom_pc,
            enable_pc_inc_b => enable_pc_inc_b
            );


    register_gen: for i in 1 to (reg_ammount)-1 generate 
       i_reg: risc_v_n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d_a => c_bus_a,
            d_b => c_bus_b,
            d_r => r_bus,
            load_a => regs_load_a(i),
            load_b => regs_load_b(i),
            load_r => regs_load_r(i),
            q => regs_q(i)
                );
    end generate register_gen;
    
--    last_register: risc_v_n_reg generic map(N => register_word_size)
--        port map(
--            clk => clk,
--            d_a => c_bus_a,
--            d_b => c_bus_b,
--            d_r => r_bus,
--            load_a => regs_load_a(31),
--            load_b => regs_load_b(31),
--            load_r => regs_load_r(31),
--            q => regs_q(31)    
--        );

    pc_adder: N_bit_adder_organic generic map(N => register_word_size)
        port map(
            input_a => pc_imm,
            input_b => pcin,
            output => pc_pre_input
            );



end behav;

