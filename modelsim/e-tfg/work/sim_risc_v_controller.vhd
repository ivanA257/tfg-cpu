-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 30.11.2020 02:55:40 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_risc_v_decoder is
    generic (
        N : integer := 32 
    );
end tb_risc_v_decoder;

architecture tb of tb_risc_v_decoder is

    component risc_v_decoder
    generic (
        N : integer := 32 
    );
        port (clk                : in std_logic;
              instruction        : in std_logic_vector (n-1 downto 0);
              outer_imm_u        : out std_logic_vector (n-1 downto 0);
              enable_imm_u_a     : out std_logic;
              enable_pc_a        : out std_logic;
              alu_mux            : out std_logic;
              alu_funct3         : out std_logic_vector (2 downto 0);
              enable_pc_inc_a    : out std_logic;
              pc_adder           : out std_logic_vector (n-1 downto 0);
              jump               : out std_logic;
              enable_md          : out std_logic;
              load_md            : out std_logic;
              funct7_significant : out std_logic;
              out_imm            : out std_logic_vector (n-1 downto 0);
              jump_enable        : out std_logic;
              w_enable_a         : out std_logic_vector (31 downto 0);
              w_enable_b         : out std_logic_vector (31 downto 0);
              w_load_a           : out std_logic_vector (31 downto 0);
              w_load_b           : out std_logic_vector (31 downto 0);
              w_load_r           : out std_logic_vector (31 downto 0));
    end component;

    signal clk                : std_logic;
    signal instruction        : std_logic_vector (n-1 downto 0);
    signal outer_imm_u        : std_logic_vector (n-1 downto 0);
    signal enable_imm_u_a     : std_logic;
    signal enable_pc_a        : std_logic;
    signal alu_mux            : std_logic;
    signal alu_funct3         : std_logic_vector (2 downto 0);
    signal enable_pc_inc_a    : std_logic;
    signal pc_adder           : std_logic_vector (n-1 downto 0);
    signal jump               : std_logic;
    signal enable_md          : std_logic;
    signal load_md            : std_logic;
    signal funct7_significant : std_logic;
    signal out_imm            : std_logic_vector (n-1 downto 0);
    signal jump_enable        : std_logic;
    signal w_enable_a         : std_logic_vector (31 downto 0);
    signal w_enable_b         : std_logic_vector (31 downto 0);
    signal w_load_a           : std_logic_vector (31 downto 0);
    signal w_load_b           : std_logic_vector (31 downto 0);
    signal w_load_r           : std_logic_vector (31 downto 0);

    constant TbPeriod : time := 50 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : risc_v_decoder generic map(N=>N)
    port map (clk                => clk,
              instruction        => instruction,
              outer_imm_u        => outer_imm_u,
              enable_imm_u_a     => enable_imm_u_a,
              enable_pc_a        => enable_pc_a,
              alu_mux            => alu_mux,
              alu_funct3         => alu_funct3,
              enable_pc_inc_a    => enable_pc_inc_a,
              pc_adder           => pc_adder,
              jump               => jump,
              enable_md          => enable_md,
              load_md            => load_md,
              funct7_significant => funct7_significant,
              out_imm            => out_imm,
              jump_enable        => jump_enable,
              w_enable_a         => w_enable_a,
              w_enable_b         => w_enable_b,
              w_load_a           => w_load_a,
              w_load_b           => w_load_b,
              w_load_r           => w_load_r);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        instruction <= "00000010000000000000011000010011";
	wait for TbPeriod;

	instruction <= "00000010000000000000011010010011";
	wait for TbPeriod;

	instruction <= "00000000110101100000011100110011";
	wait for TbPeriod;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_risc_v_decoder of tb_risc_v_decoder is
    for tb
    end for;
end cfg_tb_risc_v_decoder;