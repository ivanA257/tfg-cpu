-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 30.11.2020 02:16:07 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_risc_v_N_decoder is
generic (
    m : integer :=5
);
end tb_risc_v_N_decoder;

architecture tb of tb_risc_v_N_decoder is

    component risc_v_N_decoder
generic (
    m : integer :=2
);
        port (input  : in std_logic_vector (m-1 downto 0);
              enable : in std_logic;
              output : out std_logic_vector ((2**m)-1 downto 0));
    end component;

    signal input  : std_logic_vector (m-1 downto 0);
    signal enable : std_logic;
    signal output : std_logic_vector ((2**m)-1 downto 0);

begin

    dut : risc_v_N_decoder generic map (m=>m)
    port map (input  => input,
              enable => enable,
              output => output);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        input <= (others => '0');
        enable <= '1';
        wait for 10ns; 
        enable <= '0';
        wait for 10ns; 
        enable <= '1';
        wait for 10ns; 
        input <= "10000";

        -- EDIT Add stimuli here

        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_risc_v_N_decoder of tb_risc_v_N_decoder is
    for tb
    end for;
end cfg_tb_risc_v_N_decoder;