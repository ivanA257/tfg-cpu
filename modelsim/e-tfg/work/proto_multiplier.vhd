----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.10.2020 13:43:38
-- Design Name: 
-- Module Name: proto_multiplier - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity proto_multiplier is
generic(
    N : integer := 7);  -- I'm using seven bits because I need numbers up to 99.
Port ( 
    input1 : in  std_logic_vector(N-1 downto 0);
    input2 : in  std_logic_vector(N-1 downto 0);
    mult   : out std_logic_vector(N-1 downto 0)
);
end proto_multiplier;

architecture Behavioral of proto_multiplier is
    
    signal s_input1 : unsigned(N-1 downto 0);
    signal s_input2 : unsigned(N-1 downto 0);
    signal s_mult    : unsigned(2*N-1 downto 0);
    signal premult  : unsigned(N-1 downto 0);
begin
    
    s_input1 <= unsigned(input1);
    s_input2 <= unsigned(input2);
    
    s_mult <= s_input1 * s_input2;
    premult <= s_mult(N-1 downto 0);
    mult <= std_logic_vector(premult);

end Behavioral;
