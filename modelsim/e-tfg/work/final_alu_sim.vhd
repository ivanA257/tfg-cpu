library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.env.stop;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity final_alu_sim is
generic (
        N : integer := 7
    );
end final_alu_sim;

architecture Behavioral of final_alu_sim is
    
    component hack_alu is
        generic (
            N : integer := 4
        );
    port (
        i_x : in std_logic_vector (N-1 downto 0);
        i_y : in std_logic_vector (N-1 downto 0);
        zx : in std_logic;
        nx : in std_logic;
        zy : in std_logic;
        ny : in std_logic;
        f : in std_logic;
        no : in std_logic;
        output : out std_logic_vector (N-1 downto 0);
        zr : out std_logic;
        ng : out std_logic;
        o_c : out std_logic
    );
    end component;
    
    signal i_x    : std_logic_vector (N-1 downto 0);
    signal i_y    : std_logic_vector (N-1 downto 0);
    signal zx     : std_logic;
    signal nx     : std_logic;
    signal zy     : std_logic;
    signal ny     : std_logic;
    signal f      : std_logic;
    signal no     : std_logic;
    signal output : std_logic_vector (N-1 downto 0);
    signal zr     : std_logic;
    signal ng     : std_logic;
    signal o_c    : std_logic;
    constant period : time := 50ns;
    signal stop_condition : boolean;
    
begin

    uut : hack_alu generic map (N => N)
    port map (i_x    => i_x,
              i_y    => i_y,
              zx     => zx,
              nx     => nx,
              zy     => zy,
              ny     => ny,
              f      => f,
              no     => no,
              output => output,
              zr     => zr,
              ng     => ng,
              o_c    => o_c);
    
    stimuli : process
    begin

i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 001100" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0100001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 001100" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 001100" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 001100" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 001100" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 001100" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 001100" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 001100" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0110110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 001100" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 001100" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 001100" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 001100" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 001100" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0100000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 001100" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 001100" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0110011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 001100" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 001100" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 001100" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0101110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 001100" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0100010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 001100" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 110000" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 110000" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 110000" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0110001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 110000" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0110001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 110000" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0101010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 110000" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0100011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 110000" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000000") and (zr = '1') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 110000" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 110000" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 110000" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 110000" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 110000" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0110000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 110000" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0111000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 110000" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 110000" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 110000" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 110000" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0110100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 110000" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 110000" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 110000" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1100001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 001101" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1011110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 001101" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1000101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 001101" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1110110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 001101" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 001101" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 001101" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 001101" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1000101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 001101" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1001001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 001101" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 001101" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 001101" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1111011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 001101" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1100111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 001101" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1011111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 001101" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1100001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 001101" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1001100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 001101" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1111100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 001101" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1110000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 001101" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1010001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 001101" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1011101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 001101" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1100011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 110001" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1100100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 110001" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 110001" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1001110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 110001" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1001110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 110001" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1010101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 110001" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1011100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 110001" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1111111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 110001" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1110100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 110001" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1101010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 110001" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1111110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 110001" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1110001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 110001" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1001111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 110001" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1000111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 110001" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1110000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 110001" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1110001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 110001" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1111001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 110001" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1001011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 110001" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1111000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 110001" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '0';
no <= '1';
wait for period;
assert ((output = "1100100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 110001" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 001111" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 001111" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1000110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 001111" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 001111" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 001111" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 001111" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 001111" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1000110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 001111" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1001010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 001111" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 001111" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 001111" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 001111" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 001111" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 001111" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 001111" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1001101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 001111" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 001111" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 001111" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1010010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 001111" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '1';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 001111" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 110011" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 110011" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 110011" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1001111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 110011" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1001111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 110011" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1010110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 110011" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 110011" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0000000") and (zr = '1') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 110011" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 110011" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 110011" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 110011" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 110011" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1010000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 110011" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1001000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 110011" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 110011" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 110011" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 110011" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1001100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 110011" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 110011" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '1';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 110011" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 000010" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0111100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 000010" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1010000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 000010" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 000010" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1000110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 000010" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1000001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 000010" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0110101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 000010" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 000010" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1000001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 000010" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0101100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 000010" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0010001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 000010" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0010010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 000010" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1001000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 000010" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1011000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 000010" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0101101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 000010" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1000001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 000010" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0001001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 000010" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "1000011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 000010" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0110101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 000010" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '1';
no <= '0';
wait for period;
assert ((output = "0111101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 000010" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 010011" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0000110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 010011" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0100100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 010011" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 010011" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1100100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 010011" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 010011" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101111") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 010011" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 010011" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0101011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 010011" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 010011" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0001111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 010011" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 010011" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 010011" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1101000") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 010011" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0001111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 010011" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0100101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 010011" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 010011" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 010011" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0100111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 010011" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '0';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0000111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 010011" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 000111" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111010") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 000111" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011100") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 000111" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0101000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 000111" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0011100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 000111" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0010011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 000111" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0010001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 000111" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1000110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 000111" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1010101") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 000111" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111110") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 000111" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 000111" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0001010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 000111" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0011000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 000111" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0011000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 000111" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1110001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 000111" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011011") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 000111" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0000011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 000111" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "0100101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 000111" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1011001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 000111" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '1';
f <= '1';
no <= '1';
wait for period;
assert ((output = "1111001") and (zr = '0') and (ng = '1'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 000111" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0011100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 000000" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 000000" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 000000" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 000000" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 000000" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 000000" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 000000" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000000") and (zr = '1') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 000000" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 000000" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 000000" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000000") and (zr = '1') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 000000" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 000000" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0010000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 000000" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0100000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 000000" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0001110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 000000" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 000000" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 000000" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000100") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 000000" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 000000" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '0';
zy <= '0';
ny <= '0';
f <= '0';
no <= '0';
wait for period;
assert ((output = "0000010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 000000" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0011100"; -- i_y = 28
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0011110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 28, opcode = 010101" severity error;
i_x <= "0100001"; -- i_x = 33
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 33, y = 27, opcode = 010101" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0010110"; -- i_y = 22
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 22, opcode = 010101" severity error;
i_x <= "0001001"; -- i_x = 9
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 9, y = 49, opcode = 010101" severity error;
i_x <= "0010101"; -- i_x = 21
i_y <= "0110001"; -- i_y = 49
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0110101") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 21, y = 49, opcode = 010101" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0101010"; -- i_y = 42
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 42, opcode = 010101" severity error;
i_x <= "0010010"; -- i_x = 18
i_y <= "0100011"; -- i_y = 35
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0110011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 18, y = 35, opcode = 010101" severity error;
i_x <= "0111010"; -- i_x = 58
i_y <= "0000000"; -- i_y = 0
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111010") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 58, y = 0, opcode = 010101" severity error;
i_x <= "0110110"; -- i_x = 54
i_y <= "0001011"; -- i_y = 11
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 54, y = 11, opcode = 010101" severity error;
i_x <= "0010111"; -- i_x = 23
i_y <= "0010101"; -- i_y = 21
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0010111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 23, y = 21, opcode = 010101" severity error;
i_x <= "0010000"; -- i_x = 16
i_y <= "0000001"; -- i_y = 1
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0010001") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 16, y = 1, opcode = 010101" severity error;
i_x <= "0000100"; -- i_x = 4
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0001110") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 4, y = 14, opcode = 010101" severity error;
i_x <= "0011000"; -- i_x = 24
i_y <= "0110000"; -- i_y = 48
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 24, y = 48, opcode = 010101" severity error;
i_x <= "0100000"; -- i_x = 32
i_y <= "0111000"; -- i_y = 56
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111000") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 32, y = 56, opcode = 010101" severity error;
i_x <= "0011110"; -- i_x = 30
i_y <= "0001111"; -- i_y = 15
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0011111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 30, y = 15, opcode = 010101" severity error;
i_x <= "0110011"; -- i_x = 51
i_y <= "0001110"; -- i_y = 14
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 51, y = 14, opcode = 010101" severity error;
i_x <= "0000011"; -- i_x = 3
i_y <= "0000110"; -- i_y = 6
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0000111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 3, y = 6, opcode = 010101" severity error;
i_x <= "0001111"; -- i_x = 15
i_y <= "0110100"; -- i_y = 52
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 15, y = 52, opcode = 010101" severity error;
i_x <= "0101110"; -- i_x = 46
i_y <= "0000111"; -- i_y = 7
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0101111") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 46, y = 7, opcode = 010101" severity error;
i_x <= "0100010"; -- i_x = 34
i_y <= "0011011"; -- i_y = 27
zx <= '0';
nx <= '1';
zy <= '0';
ny <= '1';
f <= '0';
no <= '1';
wait for period;
assert ((output = "0111011") and (zr = '0') and (ng = '0'))  -- expected output
-- error will be reported if sum or carry is not 0
report "test failed for input combination x = 34, y = 27, opcode = 010101" severity error;



    stop_condition <= true;
    end process;

end Behavioral;
