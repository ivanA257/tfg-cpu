library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_adapter is
Port ( 
    clock_input : in std_logic;
    clock_output : out std_logic
);
end clock_adapter;

architecture Behavioral of clock_adapter is
    
    signal tmp : integer range 0 to 10000;
begin
    process(clock_input)
    begin
    if (clock_input'event and clock_input = '1') then
        if tmp < 10000 and tmp >= 0 then
            tmp <= tmp + 1;
        elsif tmp = 10000 then
            clock_output <= '1';
            tmp <= tmp + 1;
        else
            tmp <= 0;
            clock_output <= '0';
        end if;
    end if;
    end process;

end Behavioral;
