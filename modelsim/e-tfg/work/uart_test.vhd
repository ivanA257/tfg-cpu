-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 16.1.2021 15:40:05 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_basic_uart is
  generic (
    counter_max : integer := 3     -- Needs to be set correctly
    );
end tb_basic_uart;

architecture tb of tb_basic_uart is

    component basic_uart
        port (i_clk    : in std_logic;
              begin_tr : in std_logic;
              input    : in std_logic_vector (7 downto 0);
              output   : out std_logic);
    end component;

    signal i_clk    : std_logic;
    signal begin_tr : std_logic;
    signal input    : std_logic_vector (7 downto 0);
    signal output   : std_logic;

    constant TbPeriod : time := 50 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : basic_uart generic map(counter_max => counter_max)
    port map (i_clk    => i_clk,
              begin_tr => begin_tr,
              input    => input,
              output   => output);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that i_clk is really your main clock signal
    i_clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        begin_tr <= '0';
        input <= "10101010";
	wait for 3 * TbPeriod;
	begin_tr <= '1';
	wait for 3 * TbPeriod;
	begin_tr <= '0';
	wait for 3 * TbPeriod;
	begin_tr <= '1';

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_basic_uart of tb_basic_uart is
    for tb
    end for;
end cfg_tb_basic_uart;