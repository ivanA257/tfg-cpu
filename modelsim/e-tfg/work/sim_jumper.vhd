-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 27.11.2020 01:52:09 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_jumper is
end tb_jumper;

architecture tb of tb_jumper is

    component jumper
        port (j1     : in std_logic;
              j2     : in std_logic;
              j3     : in std_logic;
              zr     : in std_logic;
              ng     : in std_logic;
              output : out std_logic);
    end component;

    signal j1     : std_logic;
    signal j2     : std_logic;
    signal j3     : std_logic;
    signal zr     : std_logic;
    signal ng     : std_logic;
    signal output : std_logic;

begin

    dut : jumper
    port map (j1     => j1,
              j2     => j2,
              j3     => j3,
              zr     => zr,
              ng     => ng,
              output => output);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        j1 <= '1';
        j2 <= '1';
        j3 <= '1';
        zr <= '0';
        ng <= '0';

        -- EDIT Add stimuli here

        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_jumper of tb_jumper is
    for tb
    end for;
end cfg_tb_jumper;