----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.10.2020 13:50:50
-- Design Name: 
-- Module Name: bcd_decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bcd_decoder is

generic(
    N : integer := 7);
Port (
    input    : in  unsigned(N-1 downto 0);
    output_0 : out std_logic_vector(3 downto 0);
    output_1 : out std_logic_vector(3 downto 0)
 );
end bcd_decoder;

architecture Behavioral of bcd_decoder is
    
    signal final : unsigned(N-1 downto 0);
    signal int_input : integer;
    
begin

    int_input <= to_integer(input);
    output_0 <= std_logic_vector(final(3 downto 0));
    output_1 <= std_logic_vector("0" & final(6 downto 4));

process(input) -- Here I'm not using it with a clock. Which is probably a bad practice. Let's try it out this way and may change it later.
begin

    case int_input is
        when 0 to 9 =>
            final <= input;
        when 10 to 19 =>
            final <= input + 6;
        when 20 to 29 =>
            final <= input + 12;
        when 30 to 39 =>
            final <= input + 18;
        when 40 to 49 =>
            final <= input + 24;
        when 50 to 59 =>
            final <= input + 30;
        when 60 to 69 =>
            final <= input + 36;
        when 70 to 79 =>
            final <= input + 42;
        when 80 to 89 =>
            final <= input + 48;
        when 90 to 99 =>
            final <= input + 54;
        when others =>
            final <= (others => 'X');
    end case;
    
    end process;


end Behavioral;
