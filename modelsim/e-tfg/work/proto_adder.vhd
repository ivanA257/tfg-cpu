library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity proto_adder is
generic(
    N : integer := 7);  -- I'm using seven bits because I need numbers up to 99.
Port ( 
    input1 : in  std_logic_vector(N-1 downto 0);
    input2 : in  std_logic_vector(N-1 downto 0);
    sum    : out std_logic_vector(N-1 downto 0)
);
end proto_adder;

architecture Behavioral of proto_adder is
    
    signal s_input1 : unsigned(N-1 downto 0);
    signal s_input2 : unsigned(N-1 downto 0);
    signal s_sum    : unsigned(N-1 downto 0);
begin

    s_input1 <= unsigned(input1);
    s_input2 <= unsigned(input2);
    
    s_sum <= s_input1 + s_input2;
   
    sum <= std_logic_vector(s_sum);

end Behavioral;

