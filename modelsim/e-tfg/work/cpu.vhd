library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cpu is
    generic(
        register_word_size : integer := 32;
        log_2_reg_ammount : integer := 2
           );
    port (
    clk : in std_logic;
    rst : in std_logic
    );
end entity cpu;

architecture behav of cpu is

    signal nowhere : std_logic;
    -- Bus
    signal c_bus : std_logic_vector (register_word_size-1 downto 0);
    
    -- Instruction
    signal instruction : std_logic_vector(register_word_size-1 downto 0);
    
    -- Register outputs
    signal mdr_q : std_logic_vector (register_word_size-1 downto 0);
    signal mar_q : std_logic_vector (register_word_size-1 downto 0);
    signal x_q : std_logic_vector (register_word_size-1 downto 0);
    signal y_q : std_logic_vector (register_word_size-1 downto 0);
    signal custom_q : std_logic_vector (register_word_size-1 downto 0);
    type std_array is array (0 to (2**log_2_reg_ammount)-1) of std_logic_vector(register_word_size-1 downto 0);
    signal regs_q : std_array;
    signal jump_reg_q : std_logic_vector (register_word_size-1 downto 0);
    
    -- Register inputs
    signal mdr_d : std_logic_vector (register_word_size-1 downto 0);
    signal mdr_memory : std_logic_vector (register_word_size-1 downto 0);
    signal y_d : std_logic_vector (register_word_size-1 downto 0);
    
    -- Register loads
    signal mar_load : std_logic;
    signal mdr_load : std_logic;
    signal x_load : std_logic;
    signal regs_load : std_logic_vector ((2**log_2_reg_ammount)-1 downto 0);
    signal jump_reg_load : std_logic;

    
    -- Register enables
    signal mdr_enable : std_logic;
    signal y_enable : std_logic;
    signal custom_enable : std_logic;
    signal regs_enable : std_logic_vector ((2**log_2_reg_ammount)-1 downto 0);
    
    -- Multiplexer things
    signal alu_x : std_logic_vector (register_word_size-1 downto 0);

    -- Control signals
    signal m_load : std_logic;
    signal kte : std_logic_vector (register_word_size-1 downto 0);
    signal alu_zx : std_logic;
    signal alu_nx : std_logic;
    signal alu_zy : std_logic;
    signal alu_ny : std_logic;
    signal alu_f : std_logic;
    signal alu_no : std_logic;
    signal w_load : std_logic_vector (15 downto 0);
    signal w_enable : std_logic_vector (15 downto 0);
    signal alu_mul : std_logic;
    signal pc_mul : std_logic;
    signal pcin : std_logic_vector (register_word_size-1 downto 0);
 
   
    -- Flags
    signal flag_zr : std_logic;
    signal flag_ng : std_logic;
    signal flag_o_c : std_logic;

    component n_reg is
        generic (
            N : integer := 4
                );
        port (
            d : in std_logic_vector (N-1 downto 0);      
            clk : in std_logic;
            chipsel : in std_logic;
            q : out std_logic_vector (N-1 downto 0)
        );
    end component;
    
    component pc is
        generic(
            N : integer := 16
        );
        Port (
            clk : in std_logic;
            rst : in std_logic;
            inc : in std_logic;
            input : in std_logic_vector(N-1 downto 0);
            output : out std_logic_vector(N-1 downto 0)
         );
    end component;

    component N_M_RAM is
        generic (
            N : integer := 4; -- Word size
            M : integer := 2 -- Number of bits in the direction bus. The ammount of words in the memory is M**2
                );
        port (
            d : in std_logic_vector (N-1 downto 0);      
            clk : in std_logic;
            chipsel : in std_logic_vector(M-1 downto 0); -- This is the memory address
            load : in std_logic;
            q : out std_logic_vector (N-1 downto 0)
        );
    end component;
    
    component native_memory is
        generic(
            N : integer := 16; -- Word size
            M : integer := 16; -- Address bus width
            K : integer := 10  -- log_2(Memory size)
        );
        port(
            clk  : in  std_logic;
            --en   : in  std_logic;
            we   : in  std_logic;
            --rst  : in  std_logic;
            addr : in  std_logic_vector(M-1 downto 0);
            pcin : in  std_logic_vector(M-1 downto 0);
            di   : in  std_logic_vector(N-1 downto 0);
            do   : out std_logic_vector(N-1 downto 0);
            inst : out std_logic_vector(N-1 downto 0)
        );
    end component;

    component hack_alu is
        generic (
            N : integer := 4 -- Word size in the inputs and output
        );
        port (
            i_x : in std_logic_vector (N-1 downto 0);
            i_y : in std_logic_vector (N-1 downto 0);
            zx : in std_logic;
            nx : in std_logic;
            zy : in std_logic;
            ny : in std_logic;
            f : in std_logic;
            no : in std_logic;
            output : out std_logic_vector (N-1 downto 0);
            zr : out std_logic;
            ng : out std_logic;
            o_c : out std_logic
        );
    end component;

    component control_unit is
        generic (
            N : integer := 16
        );
        port (
            clk : in std_logic;
            zr : in std_logic;
            ng : in std_logic;
            instruction : in std_logic_vector (N-1 downto 0);
            w_load : out std_logic_vector (15 downto 0);
            w_enable : out std_logic_vector (15 downto 0);
            alu : out std_logic_vector (5 downto 0);
            alu_mul : out std_logic;
            pc_mul : out std_logic;
            m_load : out std_logic;
            to_bus : out std_logic_vector (N-1 downto 0)
            );
    end component;
    
begin
    
    

    
    -- Multiplexer choosing between bus and memory for mdr
    mdr_d <= c_bus when (m_load = '1') else mdr_memory; -- If m_load = 1, then we're going to be trying to put 
                                                        -- something into the memory. Therefore we need mdr to 
                                                        -- take its data from the bus.

    -- Multiplexer choosing between kte and X for the ALU
    alu_x <= x_q;

    -- Enables for the registers
    c_bus <= mdr_q when (mdr_enable = '1') else (others => 'Z');
    c_bus <= y_q when (y_enable = '1') else (others => 'Z');
    c_bus <= custom_q when (custom_enable = '1') else (others => 'Z');

    reg_enabler: for j in 0 to (2**log_2_reg_ammount)-1 generate
        c_bus <= regs_q(j) when (regs_enable(j) = '1') else (others => 'Z');
    end generate reg_enabler;

    -- Connecting the enables to the control unit
    mdr_enable <= w_enable(1);
    y_enable <= w_enable(2);
    regs_enable <= w_enable(6 downto 3); -- TODO: Check whether the bits are assigned correctly and not upside down
    custom_enable <= w_enable(7);
    -- Connecting the loads to the control unit
    mdr_load <= w_load(1);
    mar_load <= w_load(2);
    x_load <= w_load(3);
    regs_load <= w_load(7 downto 4);
    jump_reg_load <= w_load(8);


-- This is the memory implemented by me using latches, conceptually interesting because
-- one can see how the memory is built. However, it's extremely ineficient to sytethize.
-- Thus, it's better to use a built in memory module.

--    memory: N_M_RAM generic map(N => register_word_size, M => register_word_size)
--        port map(
--            clk => clk,
--            d => mdr_q,
--            chipsel => mar_q,
--            load => m_load,
--            q => mdr_memory
--                ); 
                
    native_mem: native_memory generic map( N=> register_word_size, M => register_word_size)
        port map(
            clk => clk,
            we => m_load,
            addr => mar_q,
            pcin => pcin,
            di => mdr_q,
            do => mdr_memory,
            inst => instruction
        );

    -- Implementing the program counter
    
    pc_r: pc generic map(N => register_word_size)
        port map(
            clk => clk,
            rst => rst,
            inc => pc_mul,
            input => jump_reg_q,
            output => pcin
        );

    alu: hack_alu generic map(N => register_word_size)
        port map(
            i_x => alu_x,
            i_y => c_bus,
            zx => alu_zx,
            nx => alu_nx,
            zy => alu_zy,
            ny => alu_ny,
            f => alu_f,
            no => alu_no,
            output => y_d,
            zr => flag_zr,
            ng => flag_ng,
            o_c => flag_o_c
                );

    control: control_unit 
        generic map(N => register_word_size)
        port map(
            clk => clk,
            zr => flag_zr,
            ng => flag_ng,
            instruction => instruction,
            w_load => w_load,
            w_enable => w_enable,
            alu(5) => alu_zx,
            alu(4) => alu_nx,
            alu(3) => alu_zy,
            alu(2) => alu_ny,
            alu(1) => alu_f,
            alu(0) => alu_no,
            alu_mul => alu_mul,
            pc_mul => pc_mul,
            m_load => m_load,
            to_bus => kte
                );

    mar: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => c_bus,
            chipsel => mar_load,
            q => mar_q
                );

    mdr: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => mdr_d,
            chipsel => '1',
            q => mdr_q
                );

    x: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => c_bus,
            chipsel => x_load,
            q => x_q
                );
                
    y: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => y_d,
            chipsel => '1', -- I'm not quite sure about this. Maybe the alu should notify when the result is ready. Que va, hay que cambiarlo
            q => y_q
                );
                
    custom: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => kte,
            chipsel => alu_mul,
            q => custom_q
                );
                
    jump_reg: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => c_bus,
            chipsel => jump_reg_load,
            q => jump_reg_q
                );
    

    register_gen: for i in 0 to (2**log_2_reg_ammount)-1 generate 
       i_reg: n_reg generic map(N => register_word_size)
        port map(
            clk => clk,
            d => c_bus,
            chipsel => regs_load(i),
            q => regs_q(i)
                );
    end generate register_gen;


end behav;

 
