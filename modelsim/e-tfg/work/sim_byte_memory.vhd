library ieee;
use ieee.std_logic_1164.all;

entity tb_native_memory_ble is
  generic(
    N : integer := 32; -- Word size
    M : integer := 32; -- Address bus width
    K : integer := 10  -- log_2(Memory size)
  );
end tb_native_memory_ble;

architecture tb of tb_native_memory_ble is

    component native_memory_ble
  generic(
    N : integer := 32; -- Word size
    M : integer := 32; -- Address bus width
    K : integer := 10  -- log_2(Memory size)
  );
        port (clk   : in std_logic;
              we    : in std_logic;
              addr  : in std_logic_vector (m-1 downto 0);
              pcin  : in std_logic_vector (m-1 downto 0);
              di    : in std_logic_vector (n-1 downto 0);
              width : in std_logic_vector (1 downto 0);
              unsig : in std_logic;
              do    : out std_logic_vector (n-1 downto 0);
              inst  : out std_logic_vector (n-1 downto 0));
    end component;

    signal clk   : std_logic;
    signal we    : std_logic;
    signal addr  : std_logic_vector (m-1 downto 0);
    signal pcin  : std_logic_vector (m-1 downto 0);
    signal di    : std_logic_vector (n-1 downto 0);
    signal width : std_logic_vector (1 downto 0);
    signal unsig : std_logic;
    signal do    : std_logic_vector (n-1 downto 0);
    signal inst  : std_logic_vector (n-1 downto 0);

    constant TbPeriod : time := 50 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : native_memory_ble generic map (N=>N, M=>M, K=>K)
    port map (clk   => clk,
              we    => we,
              addr  => addr,
              pcin  => pcin,
              di    => di,
              width => width,
              unsig => unsig,
              do    => do,
              inst  => inst);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        we <= '1';
        addr <= x"00000000";
        pcin <= (others => '0');
        di <= x"fffffff0";
        width <= "00";
        unsig <= '0';

	wait for 2* TbPeriod;

	addr <= x"00000001";  -- Testing the byte in different positions 
	wait for 2*TbPeriod;
	addr <= x"00000002";
	wait for 2*TbPeriod;
	addr <= x"00000003";
	wait for 2*TbPeriod;

        width <= "01";
	addr <= x"00000000";  -- Testing the half in different positions 
	wait for 2*TbPeriod;
	addr <= x"00000001";
	wait for 2*TbPeriod;
	addr <= x"00000002";
	wait for 2*TbPeriod;
	addr <= x"00000003";
	wait for 2*TbPeriod;

        width <= "10";
	addr <= x"00000000";  -- Testing the word in different positions 
	wait for 2*TbPeriod;
	addr <= x"00000001";
	wait for 2*TbPeriod;
	addr <= x"00000002";
	wait for 2*TbPeriod;
	addr <= x"00000003";
	wait for 2*TbPeriod;

        unsig <= '1';         -- Now the same thing but with unsigned
        width <= "00";
	addr <= x"00000000";
	wait for 2*TbPeriod;
	addr <= x"00000001";  -- Testing the byte in different positions 
	wait for 2*TbPeriod;
	addr <= x"00000002";
	wait for 2*TbPeriod;
	addr <= x"00000003";
	wait for 2*TbPeriod;

        width <= "01";
	addr <= x"00000000";  -- Testing the half in different positions 
	wait for 1.5*TbPeriod;
	addr <= x"00000001";
	wait for 1.5*TbPeriod;
	addr <= x"00000002";
	wait for 1.5*TbPeriod;
	addr <= x"00000003";
	wait for 1.5*TbPeriod;

        width <= "10";
	addr <= x"00000000";  -- Testing the word in different positions 
	wait for 2*TbPeriod;
	addr <= x"00000001";
	wait for 2*TbPeriod;
	addr <= x"00000002";
	wait for 2*TbPeriod;
	addr <= x"00000003";
	wait for 2*TbPeriod;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_native_memory_ble of tb_native_memory_ble is
    for tb
    end for;
end cfg_tb_native_memory_ble;