library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hello_2 is
    Port ( input1 : in STD_LOGIC;
           input2 : in STD_LOGIC;
           output : out STD_LOGIC);
end hello_2;

architecture Behavioral of hello_2 is
    
begin
    output <= input1 and input2;

end Behavioral;

