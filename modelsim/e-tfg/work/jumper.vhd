library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity jumper is
    Port (
        j1 : in std_logic;
        j2 : in std_logic;
        j3 : in std_logic;
        zr : in std_logic;
        ng : in std_logic;
        output : out std_logic
    );
end jumper;

architecture Behavioral of jumper is



begin

    process(j1, j2, j3, zr, ng)
    begin
        if (j1 = '0' and j2 = '0' and j3 = '1' and zr = '0' and ng = '0') then
            output <= '1';
        elsif (j1 = '0' and j2 = '1' and j3 = '0' and zr = '1' and ng = '0') then
            output <= '1';
        elsif (j1 = '0' and j2 = '1' and j3 = '1' and ng = '0') then
            output <= '1';
        elsif (j1 = '1' and j2 = '0' and j3 = '0' and zr = '0' and ng = '1') then
            output <= '1';
        elsif (j1 = '1' and j2 = '0' and j3 = '1' and zr = '0') then
            output <= '1';
        elsif (j1 = '1' and j2 = '1' and j3 = '0' and zr = '1' and ng = '0') then
            output <= '1';
        elsif (j1 = '1' and j2 = '1' and j3 = '0' and zr = '0' and ng = '1') then
            output <= '1';
        elsif (j1 = '1' and j2 = '1' and j3 = '1') then
            output <= '1';
        else
            output <= '0';
        end if; 
    end process;


end Behavioral;

--architecture alt of jumper is
    
--    signal jumping : std_logic_vector(4 downto 0);
    
    
--begin
    
--    jumping(4) <= j1;
--    jumping(3) <= j2;
--    jumping(2) <= j3;
--    jumping(1) <= zr;
--    jumping(0) <= ng;
    
--    process(jumping)
--    begin
--        case jumping is
--            when "00100" => output <= '1';
--            when "01010" => output <= '1';
--            when "01110" => output <= '1';
--            when "01100" => output <= '1';
--            when "10001" => output <= '1';
--            when "10101" => output <= '1';
--            when "10100" => output <= '1';
--            when "11010" => output <= '1';
--            when "11001" => output <= '1';
--            when "11100" => output <= '1';
--            when "11101" => output <= '1';
--            when "11110" => output <= '1';
--            when "11111" => output <= '1';
--            when others => output <= '0';
--        end case;
--    end process;

--end alt;
