library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main_alu is
Port ( 
    clk           : in std_logic;
    count1Button1 : in std_logic;
    count1Button2 : in std_logic;
    count2Button1 : in std_logic;
    count2Button2 : in std_logic;
    zx : in std_logic;
    nx : in std_logic;
    zy : in std_logic;
    ny : in std_logic;
    f : in std_logic;
    no : in std_logic;
    zr : out std_logic;
    ng : out std_logic;
    o_c : out std_logic;
    to_dispo      : out std_logic_vector(6 downto 0);
    chipselo      : out std_logic_vector(5 downto 0)
);
end main_alu;

architecture Behavioral of main_alu is

    component hack_alu is
        generic (
            N : integer :=7
        );
        port (
            i_x : in std_logic_vector (N-1 downto 0);
            i_y : in std_logic_vector (N-1 downto 0);
            zx : in std_logic;
            nx : in std_logic;
            zy : in std_logic;
            ny : in std_logic;
            f : in std_logic;
            no : in std_logic;
            output : out std_logic_vector (N-1 downto 0);
            zr : out std_logic;
            ng : out std_logic;
            o_c : out std_logic
        );
    end component;
    
    component clock_adapter is
        Port ( 
            clock_input : in std_logic;
            clock_output : out std_logic
        );
    end component;
    
    component counter_buttons is
        Port (
            clk      : in std_logic;
            increase : in std_logic;
            decrease : in std_logic;
            output   : out std_logic_vector(6 downto 0)
            );
    end component;
    
    component bcd_decoder is
        generic(
            N : integer := 7);
        Port (
            input    : in  unsigned(N-1 downto 0);
            output_0 : out std_logic_vector(3 downto 0);
            output_1 : out std_logic_vector(3 downto 0)
        );
    end component;
    
    component decoder_7 is
        port(
            i : in std_logic_vector(3 downto 0);
            o : out std_logic_vector(7 downto 0) -- Tiene 8 saildas porque hay una que siempre es 1 (la del punto)
	   );
    end component;
    
    component proto_barridos is
        Port (
            clk       : in std_logic;
            decoded_5 : in std_logic_vector(6  downto 0);
            decoded_4 : in std_logic_vector(6  downto 0);
            decoded_3 : in std_logic_vector(6  downto 0);
            decoded_2 : in std_logic_vector(6  downto 0);
            decoded_1 : in std_logic_vector(6  downto 0);
            decoded_0 : in std_logic_vector(6  downto 0);
            to_disp   : out std_logic_vector(6 downto 0);
            chipsel   : out std_logic_vector(5 downto 0) -- With this array we can select the display which is going to be activated. 
         );
    end component;
    
    signal count1Output : std_logic_vector (6 downto 0);
    signal count2Output : std_logic_vector (6 downto 0);
    signal selection    : std_logic;
    
    signal number11     : std_logic_vector (3 downto 0);
    signal number12     : std_logic_vector (3 downto 0);
    signal decoded11    : std_logic_vector (7 downto 0);
    signal decoded12    : std_logic_vector (7 downto 0);
    
    signal number21     : std_logic_vector (3 downto 0);
    signal number22     : std_logic_vector (3 downto 0);
    signal decoded21    : std_logic_vector (7 downto 0);
    signal decoded22    : std_logic_vector (7 downto 0);
    
    signal finalNumber  : std_logic_vector (6 downto 0);
    signal number31     : std_logic_vector (3 downto 0);
    signal number32     : std_logic_vector (3 downto 0);
    signal decoded31    : std_logic_vector (7 downto 0);
    signal decoded32    : std_logic_vector (7 downto 0);
    
    signal chipseloo    : std_logic_vector(5 downto 0);
    signal clk_adapted  : std_logic;
begin
    -- Managing the input buttons
    count1:counter_buttons port map(clk => clk, increase => count1Button1, decrease => count1Button2, output => count1Output);
    count2:counter_buttons port map(clk => clk, increase => count2Button1, decrease => count2Button2, output => count2Output);
    
    -- Displaying the first 7 bit number
    bcd1:bcd_decoder port map (input => unsigned(count1Output), output_0 => number12, output_1 => number11);
    sevdec11:decoder_7 port map (i => number11, o => decoded11);
    sevdec12:decoder_7 port map (i => number12, o => decoded12);
    
    -- Displaying the second 7 bit number
    bcd2:bcd_decoder port map (input => unsigned(count2Output), output_0 => number22, output_1 => number21);
    sevdec21:decoder_7 port map (i => number21, o => decoded21);
    sevdec22:decoder_7 port map (i => number22, o => decoded22);
    
    --Displaying the last 7 bit number (the result)
    bcd3:bcd_decoder port map (input => unsigned(finalNumber), output_0 => number32, output_1 => number31);
    sevdec31:decoder_7 port map (i => number31, o => decoded31);
    sevdec32:decoder_7 port map (i => number32, o => decoded32);
    
    --decoded11 <= "10011111";
    --decoded12 <= "00100101";
    --decoded21 <= "00001101";
    --decoded22 <= "10011001";
    --decoded31 <= "01001001";
    --decoded32 <= "01000001";
    
    -- Clock adapter
    clka:clock_adapter port map(clock_input => clk, clock_output => clk_adapted);
    
    -- Connecting the decoded numbers to the controller
    ctr:proto_barridos port map (clk => clk_adapted,
                           decoded_5 => decoded11(7 downto 1), -- The reason to take the decoded from 7
                           decoded_4 => decoded12(7 downto 1), -- to 1 is that the least significant bit
                          decoded_3 => decoded21(7 downto 1), -- in the decoder output is the bit for 
                           decoded_2 => decoded22(7 downto 1), -- the point, which we're not using here.
                           decoded_1 => decoded31(7 downto 1),
                           decoded_0 => decoded32(7 downto 1),
                           to_disp   => to_dispo,
                           chipsel  => chipselo);
                           
    -- Connect the ALU
    alu:hack_alu generic map(N => 7)
                 port map (i_x => count1Output,
                           i_y => count2Output,
                           zx  => zx,
                           nx  => nx,
                           zy  => zy,
                           ny  => ny,
                           f   => f,
                           no  => no,
                           output => finalNumber,
                           zr  => zr,
                           ng  => ng,
                           o_c => o_c);
    
end Behavioral;
