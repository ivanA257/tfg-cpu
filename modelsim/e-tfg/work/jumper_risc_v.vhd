library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity jumper_risc_v is
    generic( N : integer := 32);
    Port (
        funct3: in std_logic_vector (2 downto 0);
        a : in std_logic_vector (N-1 downto 0);
        b : in std_logic_vector (N-1 downto 0);
        branch_enable : in std_logic;
        output : out std_logic
    );
end jumper_risc_v;

architecture Behavioral of jumper_risc_v is

    signal test_is_true : std_logic;

begin

    output <= branch_enable and test_is_true;


    process(funct3, a, b)
    begin

            case funct3(2 downto 1) is

                when "00" =>   -- EQ and NE
                    if a = b then
                        test_is_true <= NOT funct3(0);
                    else
                        test_is_true <= funct3(0);
                    end if;

                when "01" =>  -- TRUE and FALSE
                    test_is_true <= NOT funct3(0);

                when "10" =>  -- SIGNED LT and GE
                    if SIGNED(a) < SIGNED(b) then
                        test_is_true <= NOT funct3(0);
                    else
                        test_is_true <= funct3(0);
                    end if;

                when "11" => -- unsigned LT and GE
                    if UNSIGNED(a) < UNSIGNED(b) then
                        test_is_true <= NOT funct3(0);
                    else
                        test_is_true <= funct3(0);
                    end if;

                when others =>
                    test_is_true <= NOT funct3(0);
            end case;
    end process;


end Behavioral;

