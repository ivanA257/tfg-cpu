library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity simple_adder is
    port (
        input_a      : in std_logic; 
        input_b      : in std_logic;
        input_carry  : in std_logic;
        output       : out std_logic;
        output_carry : out std_logic
    );
end entity simple_adder;

architecture behav of simple_adder is
   
    signal xor_1 : std_logic;
    signal and_1 : std_logic;
    signal and_2 : std_logic;
    signal and_3 : std_logic;
    signal or_1 : std_logic;
    

begin
    
    xor_1  <= input_a xor input_b;
    output <= xor_1   xor input_carry;

    and_1 <= input_a and input_b;
    and_2 <= input_a and input_carry;
    and_3 <= input_b and input_carry;
    or_1  <= and_1   or  and_2;
    output_carry <= or_1 or and_3; 


end behav;

