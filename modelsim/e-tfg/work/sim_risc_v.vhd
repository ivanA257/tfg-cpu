-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 30.11.2020 01:09:04 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_risc_v is
generic(
            register_word_size : integer := 32;
            reg_ammount : integer := 32
           );
end tb_risc_v;

architecture tb of tb_risc_v is

    component risc_v
	generic(
            register_word_size : integer := 32;
            reg_ammount : integer := 32
           );
        port (clk : in std_logic;
              rst : in std_logic);
    end component;

    signal clk : std_logic;
    signal rst : std_logic;

    constant TbPeriod : time := 50 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : risc_v generic map(register_word_size => register_word_size, reg_ammount => reg_ammount)
    port map (clk => clk,
              rst => rst);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '1';
        wait for 60 ns;
        rst <= '0';
        wait for 10 ns;
	

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_risc_v of tb_risc_v is
    for tb
    end for;
end cfg_tb_risc_v;