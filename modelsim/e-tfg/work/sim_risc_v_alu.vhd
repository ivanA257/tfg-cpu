-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 26.12.2020 22:20:21 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_risc_v_alu is
    generic (
        N : integer := 32 
    );
end tb_risc_v_alu;

architecture tb of tb_risc_v_alu is

    component risc_v_alu
        port (funct3  : in std_logic_vector (2 downto 0);
              in_x    : in std_logic_vector (n-1 downto 0);
              in_y    : in std_logic_vector (n-1 downto 0);
              add_sub : in std_logic;
              output  : out std_logic_vector (n-1 downto 0));
    end component;

    signal funct3  : std_logic_vector (2 downto 0);
    signal in_x    : std_logic_vector (n-1 downto 0);
    signal in_y    : std_logic_vector (n-1 downto 0);
    signal add_sub : std_logic;
    signal output  : std_logic_vector (n-1 downto 0);
    constant period : time := 50 ns;
    signal stop_condition : std_logic;

begin

    dut : risc_v_alu generic map(N => N)
    port map (funct3  => funct3,
              in_x    => in_x,
              in_y    => in_y,
              add_sub => add_sub,
              output  => output);

    stimuli : process
    begin



funct3 <= "000";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011000001010001"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 12369, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111100101011"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 16171, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011011110110111"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 14263, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011001001010110"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 12886, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011010100011110"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 13598, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101100011100"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 2844, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000011010111100"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 1724, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000010010011"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 4243, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011011000100001"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 13857, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011001100010001"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 13073, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000100111100"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 4412, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010101010010111"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 10903, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011011010001111"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 13967, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111011110010"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 16114, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001101111111"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 9087, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001111110001"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 9201, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011100100011100"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 14620, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001111010110"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 9174, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001001000111001"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 4665, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010011110110101"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 10165, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010011101011111"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 10079, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000100000011111010"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 16634, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000011001111101"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 1661, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110101001001"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 7497, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000100101001010010"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 19026, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010101000110000"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 10800, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011010100001000"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 13576, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100010001111"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 6287, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000100100101010011"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 18771, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011101100001011"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 15115, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010101100010011"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 11027, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010000101111001"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 8569, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001100000000"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 8960, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010111011100111"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 12007, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010101001101111"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 10863, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011011110110101"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 14261, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010010010101010"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 9386, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011110101000100"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 15684, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011001001110011"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 12915, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010110000011110"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 11294, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111001010100"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 15956, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001011101100111"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 5991, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010101100010010"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 11026, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000110010011"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 4499, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010010000101001"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 9257, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101100001000"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 2824, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010000100000110"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 8454, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110100000011"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 7427, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001000111000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 8760, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010110100101100"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 11564, operation = addition" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000011"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 3, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111010101111101"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 4294964605, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000111111000001"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 4033, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111110111111110100"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 4294963188, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000011001010010"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 1618, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111011011011010"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 4294964954, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111111111111100"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 4294967292, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111111100011011"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 4294967067, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000001110100101"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 933, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111010110010001"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 4294964625, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000010110000110"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 1414, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001001111000101"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 5061, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111000011010011"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 4294963411, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111010010010110"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 4294964374, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000101011110001"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 2801, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000010001010111101"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 8893, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111000100000000"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 4294963456, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001110110011110"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 7582, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111110001001011"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 4294966347, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000100010000111"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 2183, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111110000111011"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 4294966331, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111101011111110"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 4294966014, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111101111010101"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 4294966229, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001001011101101"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 4845, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000010100010"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 162, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000111010100010"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 3746, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000001000110110"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 566, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111001001011001"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 4294963801, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111101111011101"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 4294966237, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000111011010111"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 3799, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111000111011111"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 4294963679, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001000000110011"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 4147, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001000001010100"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 4180, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111011110111101"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 4294965181, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111110111110100101"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 4294963109, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000100000010111"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 2071, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111010100000100"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 4294964484, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000101011010100"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 2772, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001100101100011"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 6499, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000011101011100"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 1884, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111100100101110"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 4294965550, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000100000101011"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 2091, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111110110101101100"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 4294962540, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111111001100101101"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 4294964013, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001000101001111"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 4431, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000001111010"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 122, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000010000001100000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 8288, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000001000011111111"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 4351, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111110001000011000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 4294959640, operation = subtraction" severity error;
funct3 <= "000";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '1';
wait for period;
assert ((output = "11111111111111111110000110101010"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 4294959530, operation = subtraction" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 0, operation = sll" severity error;
funct3 <= "001";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 0, operation = sll" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 0, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 1, operation = conditional" severity error;
funct3 <= "010";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 1, operation = conditional" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 0, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 1, operation = uns_condit" severity error;
funct3 <= "011";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000001"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 1, operation = uns_condit" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000001101"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 13, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111010000011"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 16003, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011000001000111"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 12359, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011000000010100"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 12308, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101011011110"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 2782, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101011011010"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 2778, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000111100"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 60, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111101101011"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 3947, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010111011101"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 1501, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101010010001"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 2705, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111010111010"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 3770, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001010001000111"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 5191, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011000101101111"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 12655, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011110011101010"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 15594, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001101101111111"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 7039, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001111001101"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 9165, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011000100000000"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 12544, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001110100110"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 9126, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000110110110101"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 3509, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001011110001001"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 6025, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010001011111"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 1119, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111100000010"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 16130, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010001111101"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 1149, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110100110101"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 7477, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000110100010"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 418, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000110101110"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 4526, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001011110110"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 758, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001011001101111"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 5743, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010000100011"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 1059, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011001011101011"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 13035, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001001011100011"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 4835, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000001110101"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 4213, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000011111100"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 4348, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100011000111"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 2247, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000001101111"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 4207, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100000101001"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 2089, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001101100000100"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 6916, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011110100110100"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 15668, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010100101100011"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 10595, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101111011100"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 3036, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011100101010010"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 14674, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100001010111"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 2135, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001001011101100"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 4844, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000110101010011"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 3411, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001001111010001"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 5073, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000010000110"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 134, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010000011100000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 8416, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000100000011"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 4355, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001000111000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 8760, operation = xor" severity error;
funct3 <= "100";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001010101010"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 8874, operation = xor" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 0, operation = srl" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 0, operation = sra" severity error;
funct3 <= "101";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '1';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 0, operation = sra" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100000101111"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 6191, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111011010111"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 16087, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011001111111111"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 13311, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011000100110101"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 12597, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111111111110"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 8190, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101011111011"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 2811, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001101111100"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 892, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111111111111"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 4095, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110111111111"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 7679, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111011010001"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 7889, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111111111011"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 4091, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111101101111"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 8047, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011001111111111"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 13311, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011110111101110"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 15854, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111101111111"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 8063, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001111011111"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 9183, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011010100001110"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 13582, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001110111110"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 9150, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111111110111"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 4087, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111110011111"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 8095, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001010111011111"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 5599, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011111111111110"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 16382, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010101111101"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 1405, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110100111111"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 7487, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010010111111010"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 9722, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110111101111"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 7663, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001101111111111"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 7167, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001011101111111"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 6015, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010011010111011"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 9915, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011011011111011"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 14075, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111011111011"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 7931, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100011110111"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 6391, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100111111110"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 6654, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001101111010111"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 7127, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001110101101111"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 7535, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111111101111"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 8175, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111111010111"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 8151, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011110100111100"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 15676, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010110111101011"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 11755, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001101111111101"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 7165, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000011101111010011"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 15315, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111111011111"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 4063, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001111011111111"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 7935, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000111101110011"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 3955, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001101111111101"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 7165, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010111000111"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 1479, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010000011110011"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 8435, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001011100000011"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 5891, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001000111000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 8760, operation = or" severity error;
funct3 <= "110";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010011111101011"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 10219, operation = or" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100000101010";
in_y <= "00000000000000000001100000100111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100000100010"))  -- expected output
report "test failed for input combination x = 6186, y = 6183, expected = 6178, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001101001010100";
in_y <= "00000000000000000010010011010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000001010100"))  -- expected output
report "test failed for input combination x = 6740, y = 9431, expected = 84, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010001110111100";
in_y <= "00000000000000000001001111111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001110111000"))  -- expected output
report "test failed for input combination x = 9148, y = 5115, expected = 952, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001000100100101";
in_y <= "00000000000000000010000100110001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000100100001"))  -- expected output
report "test failed for input combination x = 4389, y = 8497, expected = 289, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001110110111000";
in_y <= "00000000000000000001011101100110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001010100100000"))  -- expected output
report "test failed for input combination x = 7608, y = 5990, expected = 5408, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000000011111011";
in_y <= "00000000000000000000101000100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000100001"))  -- expected output
report "test failed for input combination x = 251, y = 2593, expected = 33, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000001101011100";
in_y <= "00000000000000000000001101100000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001101000000"))  -- expected output
report "test failed for input combination x = 860, y = 864, expected = 832, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000011111010111";
in_y <= "00000000000000000000100010111100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000010010100"))  -- expected output
report "test failed for input combination x = 2007, y = 2236, expected = 148, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001110011100011";
in_y <= "00000000000000000001100100111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100000100010"))  -- expected output
report "test failed for input combination x = 7395, y = 6462, expected = 6178, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001010001010001";
in_y <= "00000000000000000001111011000000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001010001000000"))  -- expected output
report "test failed for input combination x = 5201, y = 7872, expected = 5184, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000101101100001";
in_y <= "00000000000000000000010111011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000101000001"))  -- expected output
report "test failed for input combination x = 2913, y = 1499, expected = 321, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001111100101110";
in_y <= "00000000000000000000101101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000101100101000"))  -- expected output
report "test failed for input combination x = 7982, y = 2921, expected = 2856, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001001110110001";
in_y <= "00000000000000000010001011011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001010010000"))  -- expected output
report "test failed for input combination x = 5041, y = 8926, expected = 656, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100111000100";
in_y <= "00000000000000000010010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000100000100"))  -- expected output
report "test failed for input combination x = 6596, y = 9518, expected = 260, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001011100111000";
in_y <= "00000000000000000000110001000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010000000000"))  -- expected output
report "test failed for input combination x = 5944, y = 3143, expected = 1024, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010001101010111";
in_y <= "00000000000000000000000010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000010010"))  -- expected output
report "test failed for input combination x = 9047, y = 154, expected = 18, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001010100001110";
in_y <= "00000000000000000010010000001110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010000001110"))  -- expected output
report "test failed for input combination x = 5390, y = 9230, expected = 1038, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010000010111010";
in_y <= "00000000000000000000001100011100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000011000"))  -- expected output
report "test failed for input combination x = 8378, y = 796, expected = 24, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000011101000010";
in_y <= "00000000000000000000101011110111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001001000010"))  -- expected output
report "test failed for input combination x = 1858, y = 2807, expected = 578, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100000011110";
in_y <= "00000000000000000000111110010111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100000010110"))  -- expected output
report "test failed for input combination x = 6174, y = 3991, expected = 2070, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001000111001101";
in_y <= "00000000000000000001010110010010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000110000000"))  -- expected output
report "test failed for input combination x = 4557, y = 5522, expected = 4480, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001110111111100";
in_y <= "00000000000000000010001011111110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000011111100"))  -- expected output
report "test failed for input combination x = 7676, y = 8958, expected = 252, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000000100101001";
in_y <= "00000000000000000000010101010100";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000100000000"))  -- expected output
report "test failed for input combination x = 297, y = 1364, expected = 256, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100000011011";
in_y <= "00000000000000000000010100101110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000001010"))  -- expected output
report "test failed for input combination x = 6171, y = 1326, expected = 10, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010010101111010";
in_y <= "00000000000000000010010011011000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010010001011000"))  -- expected output
report "test failed for input combination x = 9594, y = 9432, expected = 9304, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001110001101001";
in_y <= "00000000000000000000110111000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000110001000001"))  -- expected output
report "test failed for input combination x = 7273, y = 3527, expected = 3137, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001101110011111";
in_y <= "00000000000000000001100101101001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001100100001001"))  -- expected output
report "test failed for input combination x = 7071, y = 6505, expected = 6409, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000010101110100";
in_y <= "00000000000000000001001100011011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000100010000"))  -- expected output
report "test failed for input combination x = 1396, y = 4891, expected = 272, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010001010011000";
in_y <= "00000000000000000010011010111011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000010001010011000"))  -- expected output
report "test failed for input combination x = 8856, y = 9915, expected = 8856, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010010011110001";
in_y <= "00000000000000000001011000011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010000010000"))  -- expected output
report "test failed for input combination x = 9457, y = 5658, expected = 1040, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000111001111001";
in_y <= "00000000000000000001110010011010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000110000011000"))  -- expected output
report "test failed for input combination x = 3705, y = 7322, expected = 3096, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100011010110";
in_y <= "00000000000000000000100010100011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100010000010"))  -- expected output
report "test failed for input combination x = 6358, y = 2211, expected = 2178, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100110101010";
in_y <= "00000000000000000000100101010110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100100000010"))  -- expected output
report "test failed for input combination x = 6570, y = 2390, expected = 2306, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001001101010010";
in_y <= "00000000000000000001101110010101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001001100010000"))  -- expected output
report "test failed for input combination x = 4946, y = 7061, expected = 4880, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000110100001010";
in_y <= "00000000000000000001110101100101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000110100000000"))  -- expected output
report "test failed for input combination x = 3338, y = 7525, expected = 3328, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001111111100110";
in_y <= "00000000000000000001011111001111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001011111000110"))  -- expected output
report "test failed for input combination x = 8166, y = 6095, expected = 6086, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000110011010111";
in_y <= "00000000000000000001011111010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010011010011"))  -- expected output
report "test failed for input combination x = 3287, y = 6099, expected = 1235, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010010000001100";
in_y <= "00000000000000000001100100111000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000001000"))  -- expected output
report "test failed for input combination x = 9228, y = 6456, expected = 8, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010010111101011";
in_y <= "00000000000000000000110010001000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010010001000"))  -- expected output
report "test failed for input combination x = 9707, y = 3208, expected = 1160, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001100110111101";
in_y <= "00000000000000000001001001100001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000001000000100001"))  -- expected output
report "test failed for input combination x = 6589, y = 4705, expected = 4129, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001101111000001";
in_y <= "00000000000000000010001010010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001010000001"))  -- expected output
report "test failed for input combination x = 7105, y = 8851, expected = 641, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000111111001001";
in_y <= "00000000000000000000011110011110";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000011110001000"))  -- expected output
report "test failed for input combination x = 4041, y = 1950, expected = 1928, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000110000111111";
in_y <= "00000000000000000001111011010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000110000010011"))  -- expected output
report "test failed for input combination x = 3135, y = 7891, expected = 3091, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000001001100000";
in_y <= "00000000000000000000111100110011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000001000100000"))  -- expected output
report "test failed for input combination x = 608, y = 3891, expected = 544, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001101010111100";
in_y <= "00000000000000000000100101101101";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000100000101100"))  -- expected output
report "test failed for input combination x = 6844, y = 2413, expected = 2092, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000010111000001";
in_y <= "00000000000000000000010101000111";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010101000001"))  -- expected output
report "test failed for input combination x = 1473, y = 1351, expected = 1345, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000010000010110011";
in_y <= "00000000000000000000000001010011";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000010011"))  -- expected output
report "test failed for input combination x = 8371, y = 83, expected = 19, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000001011100000001";
in_y <= "00000000000000000000011000000010";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000011000000000"))  -- expected output
report "test failed for input combination x = 5889, y = 1538, expected = 1536, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000001000101000";
in_y <= "00000000000000000010000000010000";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000000000000000"))  -- expected output
report "test failed for input combination x = 552, y = 8208, expected = 0, operation = and" severity error;
funct3 <= "111";
in_x <= "00000000000000000000011101101011";
in_y <= "00000000000000000010010111000001";
add_sub <= '0';
wait for period;
assert ((output = "00000000000000000000010101000001"))  -- expected output
report "test failed for input combination x = 1899, y = 9665, expected = 1345, operation = and" severity error;



        stop_condition <= '1';
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_risc_v_alu of tb_risc_v_alu is
    for tb
    end for;
end cfg_tb_risc_v_alu;