-- Block RAM with Resettable Data Output
-- File: rams_sp_rf_rst.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity native_memory is
  generic(
    N : integer := 16; -- Word size
    M : integer := 16; -- Address bus width
    K : integer := 10  -- log_2(Memory size)
  );
  port(
    clk  : in  std_logic;
    --en   : in  std_logic;
    we   : in  std_logic;
    --rst  : in  std_logic;
    addr : in  std_logic_vector(M-1 downto 0);
    pcin : in  std_logic_vector(M-1 downto 0);
    di   : in  std_logic_vector(N-1 downto 0);
    do   : out std_logic_vector(N-1 downto 0);
    inst : out std_logic_vector(N-1 downto 0)
  );
end native_memory;

architecture syn of native_memory is
  type ram_type is array ((2**K)-1 downto 0) of std_logic_vector(N-1 downto 0);
    signal ram : ram_type := (0 => "00000010000000000000011000010011",
                               1 => "00001100100000000000011010010011",
                               2 => "00000000110101100000011100110011",
                               3 => "00000000111000000010100000100011",
                          --    4 => "0000000001000000",
--                              5 => "1000000001100101",
--                              6 => "0111101000000000",
--                              7 => "1000000000000110",
--                              8 => "0111100101000000",
--                              9 => "0000000001000000",
--                              10 => "1000000001100100",
--                              11 => "0111101000000000",
--                              12 => "1000000000000000",
--                              13 => "0100101100000000",
--                              14 => "1000000001100101",
--                              15 => "0111101000000000",
--                              16 => "1000000000000000",
--                              17 => "0000100000000010",
                            others => (others => '0'));
begin
  do <= ram(conv_integer(addr));
  inst <= ram(conv_integer(pcin));
  process(clk)
  begin
    if clk'event and clk = '1' then
        if we = '1' then        -- write enable
          ram(conv_integer(addr)) <= di;
        end if;
    end if;
  end process;

end syn;