library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity N_decoder is
    generic (
    M : integer :=2
);
Port ( 
    input : in std_logic_vector(M-1 downto 0);
    output : out std_logic_vector((2**M)-1 downto 0)
);
end N_decoder;

architecture Behavioral of N_decoder is

begin

    process(input)
    
    begin
        for i in 0 to (2**M)-1 loop
            if unsigned(input) = i then
                output(i) <= '1';
            else
                output(i) <= '0'; 
            end if;
        end loop;
    end process;


end Behavioral;
