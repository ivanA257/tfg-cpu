library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity N_bit_adder is
    generic (
        N : integer := 7
    );
    port (
        input_a : in std_logic_vector (N-1 downto 0);
        input_b : in std_logic_vector (N-1 downto 0);
        i_carry : in std_logic;
        output  : out std_logic_vector (N-1 downto 0);
        o_carry : out std_logic
    );
end entity N_bit_adder;

architecture behav of N_bit_adder is
begin

    process (input_a, input_b, i_carry)
        variable carry : std_logic;
        variable tmps  : std_logic_vector (N-1 downto 0);
    begin
        carry := i_carry;
        for i in 0 to N-1 loop
            tmps(i) := input_a(i) xor input_b(i) xor carry;
            carry :=    (input_a(i) and input_b(i))
                     or (input_a(i) and carry)
                     or (input_b(i) and carry);
        end loop; 
        output <= tmps;
        o_carry <= carry;
    end process;  



end behav;


