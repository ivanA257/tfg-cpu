library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity N_M_RAM is
    generic (
        N : integer := 4; -- Word size
        M : integer := 2 -- Number of bits in the direction bus. The ammount of words in the memory is M**2
            );
    port (
        d : in std_logic_vector (N-1 downto 0);      
        clk : in std_logic;
        chipsel : in std_logic_vector(M-1 downto 0); -- This is the memory address
        load : in std_logic;
        q : out std_logic_vector (N-1 downto 0)
    );
end N_M_RAM;

architecture Behavioral of N_M_RAM is

    component n_reg is
        generic (
        N : integer := 4
            );
    port (
        d : in std_logic_vector (N-1 downto 0);      
        clk : in std_logic;
        chipsel : in std_logic;
        q : out std_logic_vector (N-1 downto 0)
    );
    end component;

    signal decoded_sel : std_logic_vector((2**M)-1 downto 0);
    type std_array is array (0 to (2**M)-1) of std_logic_vector(N-1 downto 0);
    signal outputs : std_array;
     
begin
    --decoded_sel <= (others => '0');
    --decoded_sel(to_integer(unsigned(chipsel))) <= '1';
    
    

    process(chipsel, load)
    
    -- This multiplexer set decides which word to activate. Therefore, where to write. 
    begin
        
        for i in 0 to (M**2)-1 loop
            if unsigned(chipsel) = i then
                decoded_sel(i) <= load; -- This way we can control if the memory gets anything loaded or not. 
            else
                decoded_sel(i) <= '0';
            end if;
        end loop;
    end process;
    
    -- This generate loop generates the words themselves
    arrai: for j in 0 to (M**2)-1 generate
        word_n: n_reg generic map (N => N) port map(
            d => d,
            clk => clk,
            chipsel => decoded_sel(j),
            q => outputs(j)
        );
    end generate arrai;

q <= outputs(to_integer(unsigned(chipsel))); -- This implements the output multiplexer, to select which word to load to the bus

end Behavioral;
