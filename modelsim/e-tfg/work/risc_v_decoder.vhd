library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity risc_v_decoder is
    generic (
        N : integer := 32 
    );
    port (
        clk : in std_logic;
        instruction : in std_logic_vector (N-1 downto 0);
        outer_imm_u : out std_logic_vector (N-1 downto 0);
        enable_imm_u_a : out std_logic := '0';
        enable_pc_a : out std_logic := '0';
        alu_mux : out std_logic := '0';
        alu_funct3 : out std_logic_vector (2 downto 0);
        enable_pc_inc_a : out std_logic;
        pc_adder : out std_logic_vector (N-1 downto 0);
        jump : out std_logic := '0'; -- This one means inconditional jump.
        enable_md : out std_logic := '0';
        load_md : out std_logic := '0';
        funct7_significant : out std_logic;
        out_imm : out std_logic_vector (N-1 downto 0);
        jump_enable : out std_logic := '0';
        w_enable_a : out std_logic_vector (31 downto 0);
        w_enable_b : out std_logic_vector (31 downto 0);
        w_load_a : out std_logic_vector (31 downto 0);
        w_load_b : out std_logic_vector (31 downto 0);
        w_load_r : out std_logic_vector (31 downto 0);
        mem_width : out std_logic_vector (1 downto 0);
        mem_unsig : out std_logic;
        custom_pc : out std_logic;
        enable_pc_inc_b : out std_logic
    );
end entity risc_v_decoder;
    
architecture behav of risc_v_decoder is

    component risc_v_N_decoder is
        generic (
            M : integer :=2
        );
        Port ( 
            input : in std_logic_vector(M-1 downto 0);
            enable : in std_logic;
            output : out std_logic_vector((2**M)-1 downto 0)
        );
    end component;
    

    signal rd : std_logic_vector (4 downto 0);
    signal rs1 : std_logic_vector (4 downto 0);
    signal rs2 : std_logic_vector (4 downto 0);
    signal funct3 : std_logic_vector (2 downto 0);
    signal opcode : std_logic_vector (4 downto 0);
    
    
    -- Enables
    signal enable_rs1_a : std_logic;
    signal enable_rs2_b : std_logic;
    
    -- Loads
    signal load_dreg_a : std_logic;
    signal load_dreg_b : std_logic;
    signal load_dreg_r : std_logic;

    -- Auxiliary constants
    signal zeros_12 : std_logic_vector (11 downto 0) := (others => '0');
    signal zeros_27 : std_logic_vector (26 downto 0) := (others => '0');
    signal zeros_20 : std_logic_vector (19 downto 0) := (others => '0');

    -- Immediate values
    signal imm_u : std_logic_vector (N-1 downto 0);
    signal imm_j : std_logic_vector (N-1 downto 0);
    signal imm_i : std_logic_vector (N-1 downto 0);
    signal imm_i_shift : std_logic_vector (N-1 downto 0);
    signal imm_s : std_logic_vector (N-1 downto 0);
    signal imm_b : std_logic_vector (N-1 downto 0);

    -- Flow control signals 
    signal enable_lui : std_logic;
    signal enable_auipc : std_logic;
    signal enable_J : std_logic;
    signal enable_R : std_logic;
    signal enable_I : std_logic;
    signal enable_S : std_logic;
    signal enable_B : std_logic;
    signal enable_jalr : std_logic;
    signal enables : std_logic_vector (7 downto 0);
    signal pre_imm_i : std_logic_vector (N-1 downto 0);
    signal we_are_in_shift : std_logic;
    
    -- �apas
    signal triple_opcode_2 : std_logic_vector (2 downto 0);

    begin
    -- Getting info from the instruction
    opcode <= instruction(6 downto 2);
    rd <= instruction(11 downto 7);
    rs1 <= instruction(19 downto 15);
    rs2 <= instruction(24 downto 20);
    funct3 <= instruction(14 downto 12);
    funct7_significant <= instruction(30) and enable_R;

    imm_u <= instruction(31 downto 12) & zeros_12;
    
    imm_j(19 downto 12) <= instruction(19 downto 12);
    imm_j(11) <= instruction(20);
    imm_j(10 downto 1) <= instruction(30 downto 21);
    imm_j(0) <= '0';
    

    -- imm_i(31 downto 12) <= zeros_20;

    imm_j(31) <= instruction(31);
    imm_j(30) <= instruction(31);
    imm_j(29) <= instruction(31);
    imm_j(28) <= instruction(31);
    imm_j(27) <= instruction(31);
    imm_j(26) <= instruction(31);
    imm_j(25) <= instruction(31);
    imm_j(24) <= instruction(31);
    imm_j(23) <= instruction(31);
    imm_j(22) <= instruction(31);
    imm_j(21) <= instruction(31);
    imm_j(20) <= instruction(31);
    
    imm_i(31) <= instruction(31);
    imm_i(30) <= instruction(31);
    imm_i(29) <= instruction(31);
    imm_i(28) <= instruction(31);
    imm_i(27) <= instruction(31);
    imm_i(26) <= instruction(31);
    imm_i(25) <= instruction(31);
    imm_i(24) <= instruction(31);
    imm_i(23) <= instruction(31);
    imm_i(22) <= instruction(31);
    imm_i(21) <= instruction(31);
    imm_i(20) <= instruction(31);
    imm_i(19) <= instruction(31);
    imm_i(18) <= instruction(31);
    imm_i(17) <= instruction(31);
    imm_i(16) <= instruction(31);
    imm_i(15) <= instruction(31);
    imm_i(14) <= instruction(31);
    imm_i(14) <= instruction(31);
    imm_i(13) <= instruction(31);
    imm_i(12) <= instruction(31);

    imm_s(31) <= instruction(31);
    imm_s(30) <= instruction(31);
    imm_s(29) <= instruction(31);
    imm_s(28) <= instruction(31);
    imm_s(27) <= instruction(31);
    imm_s(26) <= instruction(31);
    imm_s(25) <= instruction(31);
    imm_s(24) <= instruction(31);
    imm_s(23) <= instruction(31);
    imm_s(22) <= instruction(31);
    imm_s(21) <= instruction(31);
    imm_s(20) <= instruction(31);
    imm_s(19) <= instruction(31);
    imm_s(18) <= instruction(31);
    imm_s(17) <= instruction(31);
    imm_s(16) <= instruction(31);
    imm_s(15) <= instruction(31);
    imm_s(14) <= instruction(31);
    imm_s(14) <= instruction(31);
    imm_s(13) <= instruction(31);
    imm_s(12) <= instruction(31);

    imm_b(31) <= instruction(31);
    imm_b(30) <= instruction(31);
    imm_b(29) <= instruction(31);
    imm_b(28) <= instruction(31);
    imm_b(27) <= instruction(31);
    imm_b(26) <= instruction(31);
    imm_b(25) <= instruction(31);
    imm_b(24) <= instruction(31);
    imm_b(23) <= instruction(31);
    imm_b(22) <= instruction(31);
    imm_b(21) <= instruction(31);
    imm_b(20) <= instruction(31);
    imm_b(19) <= instruction(31);
    imm_b(18) <= instruction(31);
    imm_b(17) <= instruction(31);
    imm_b(16) <= instruction(31);
    imm_b(15) <= instruction(31);
    imm_b(14) <= instruction(31);
    imm_b(14) <= instruction(31);
    imm_b(13) <= instruction(31);
    imm_b(12) <= instruction(31);


    -- Rest of the bits from the constants
    imm_i(11 downto 0) <= instruction(31 downto 20);
    

    imm_i_shift <= zeros_27 & instruction(24 downto 20);

    
    imm_s(11 downto 5) <= instruction(31 downto 25);
    imm_s(4 downto 0) <= instruction(11 downto 7);

    
    imm_b(11) <= instruction(7);
    imm_b(10 downto 5) <= instruction(30 downto 25);
    imm_b(4 downto 1) <= instruction(11 downto 8);
    imm_b(0) <= '0';
    

    -- Decoding enables
    enable_a_decoder: risc_v_N_decoder generic map(M=>5)
        port map(
            input => rs1,
            enable => enable_rs1_a,
            output => w_enable_a
            );

    enable_b_decoder: risc_v_N_decoder generic map(M=>5)
        port map(
            input => rs2,
            enable => enable_rs2_b,
            output => w_enable_b
            );

    -- Decoding loads
    load_a_decoder: risc_v_N_decoder generic map(M=>5)
        port map(
            input => rd,
            enable => load_dreg_a,
            output => w_load_a
            );

    load_b_decoder: risc_v_N_decoder generic map(M=>5)
        port map(
            input => rd,
            enable => load_dreg_b,
            output => w_load_b
            );

    load_r_decoder: risc_v_N_decoder generic map(M=>5)
        port map(
            input => rd,
            enable => load_dreg_r,
            output => w_load_r
            );
            
    -- We decide which way we are going to take, based in the operation type.
    with opcode select enable_lui <= 
        '1' when "01101",
        '0' when others;
            
    with opcode select enable_auipc <= 
        '1' when "00101",
        '0' when others;
        
    with opcode select enable_J <= 
        '1' when "11011",
        '0' when others;
        
    with opcode select enable_R <= 
        '1' when "01100",
        '0' when others;
        
    with opcode select enable_I <= 
        '1' when "00000",
        '1' when "00100",
        '0' when others;
        
    with opcode select enable_S <= 
        '1' when "01000",
        '0' when others;
        
    with opcode select enable_B <= 
        '1' when "11000",
        '0' when others;
        
    with opcode select enable_jalr <=
        '1' when "11001",
        '0' when others;

    we_are_in_shift <= opcode(2) and not funct3(1) and funct3(0);

    -- Put all the flow control signals into a single one.
    enables <= enable_lui & enable_auipc & enable_J & enable_R & enable_I & enable_S & enable_B & enable_jalr;

    -- Then we tell him what happens in each of the cases. 
    enable_imm_u_a <= enable_lui;
    outer_imm_u <= imm_u;
    
    load_dreg_a <= enable_lui or enable_J;
    load_dreg_b <= (enable_I and not opcode(2)) or enable_jalr;
    load_dreg_r <= enable_auipc or enable_R or (enable_I and opcode(2));
    
    enable_pc_a <= enable_auipc;

    alu_mux <= enable_auipc or enable_I or enable_S or enable_jalr; -- It has to be zero also in R

    with enables select out_imm <= 
        imm_u     when "01000000",
        pre_imm_i when "00001000",
        imm_s     when "00000100",
        imm_i     when "00000001",
        (others => '0') when others;

    with we_are_in_shift select pre_imm_i <=
        imm_i_shift when '1',
        imm_i       when others;

    triple_opcode_2(0) <= opcode(2);
    triple_opcode_2(1) <= opcode(2);
    triple_opcode_2(2) <= opcode(2);
    
    with enables select alu_funct3 <= 
        "000"  when "01000000", -- AUIPC case
        funct3 when "00010000", -- R case
        (funct3 and triple_opcode_2) when "00001000", -- I case
        funct3 when "00000010",
        "000"  when others;

    enable_pc_inc_a <= enable_J;
    
    with enables select pc_adder <= 
        imm_j when "00100000", -- J case
        imm_b when "00000010", -- B case
        (others => '0') when others;

    jump <= enable_J or enable_jalr;

    enable_rs1_a <= enable_R or enable_I or enable_B or enable_S or enable_jalr;
    enable_rs2_b <= enable_R or enable_B or enable_S;

    enable_md <= enable_I and not opcode(2);
    load_md <= enable_S;
    jump_enable <= enable_B;
    
    -- Memory control
    mem_width <= funct3(1 downto 0);
    mem_unsig <= funct3(2);
    
    custom_pc <= enable_jalr;
    enable_pc_inc_b <= enable_jalr;


end behav;
 
