library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity final_alu_sim is
generic (
        N : integer := 4
    );
end final_alu_sim;

architecture Behavioral of final_alu_sim is
    
    component hack_alu is
        generic (
            N : integer := 4
        );
    port (
        i_x : in std_logic_vector (N-1 downto 0);
        i_y : in std_logic_vector (N-1 downto 0);
        zx : in std_logic;
        nx : in std_logic;
        zy : in std_logic;
        ny : in std_logic;
        f : in std_logic;
        no : in std_logic;
        output : out std_logic_vector (N-1 downto 0);
        zr : out std_logic;
        ng : out std_logic;
        o_c : out std_logic
    );
    end component;
    
    signal i_x    : std_logic_vector (N-1 downto 0);
    signal i_y    : std_logic_vector (N-1 downto 0);
    signal zx     : std_logic;
    signal nx     : std_logic;
    signal zy     : std_logic;
    signal ny     : std_logic;
    signal f      : std_logic;
    signal no     : std_logic;
    signal output : std_logic_vector (N-1 downto 0);
    signal zr     : std_logic;
    signal ng     : std_logic;
    signal o_c    : std_logic;
    
begin

    uut : hack_alu generic map (N => N)
    port map (i_x    => i_x,
              i_y    => i_y,
              zx     => zx,
              nx     => nx,
              zy     => zy,
              ny     => ny,
              f      => f,
              no     => no,
              output => output,
              zr     => zr,
              ng     => ng,
              o_c    => o_c);
    
    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        i_x <= (others => '0');
        i_y <= (others => '0');
        zx <= '0';
        nx <= '0';
        zy <= '0';
        ny <= '0';
        f <= '0';
        no <= '0';

        -- EDIT Add stimuli here

        wait;
    end process;

end Behavioral;
