library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity hack_alu is
    generic (
        N : integer := 4
    );
    port (
        i_x : in std_logic_vector (N-1 downto 0);
        i_y : in std_logic_vector (N-1 downto 0);
        zx : in std_logic;
        nx : in std_logic;
        zy : in std_logic;
        ny : in std_logic;
        f : in std_logic;
        no : in std_logic;
        output : out std_logic_vector (N-1 downto 0);
        zr : out std_logic;
        ng : out std_logic;
        o_c : out std_logic
    );
end entity hack_alu;

architecture behav of hack_alu is

    component N_bit_adder_organic is
        generic (
            N : integer := 4
        );
        port (
            input_a : in std_logic_vector (N-1 downto 0);
            input_b : in std_logic_vector (N-1 downto 0);
            output  : out std_logic_vector (N-1 downto 0);
            o_carry : out std_logic
        );
    end component;
    

    signal zero_x : std_logic_vector (N-1 downto 0);
    signal neg_x  : std_logic_vector (N-1 downto 0);
    signal zero_y : std_logic_vector (N-1 downto 0);
    signal neg_y : std_logic_vector (N-1 downto 0);
    signal post_f : std_logic_vector (N-1 downto 0);
    signal neg_out : std_logic_vector (N-1 downto 0);
    signal sum_xy : std_logic_vector (N-1 downto 0);
    signal and_xy : std_logic_vector (N-1 downto 0);
    signal pre_output : std_logic_vector (N-1 downto 0);
    signal zeroes : std_logic_vector (N-1 downto 0) := (others =>'0');

begin

    adder:N_bit_adder_organic generic map(N => N) port map(
        input_a => neg_x,
        input_b => neg_y,
        output  => sum_xy,
        o_carry => o_c
        );

    with zx select zero_x <=
        i_x            when '0',
        (others => '0') when others;

    with nx select neg_x <=
        not zero_x when '1',
        zero_x     when others;

    with zy select zero_y <=
        i_y            when '0',
        (others => '0') when others;

    with ny select neg_y <=
        not zero_y when '1',
        zero_y     when others;

    with f select post_f <=
        sum_xy when '1',
        and_xy when others;
    
        
   with no select pre_output <= 
        not post_f when '1',
        post_f     when others;

    ng <= pre_output(N-1);
    output <= pre_output;

    

    process (pre_output)
    begin
        
        if pre_output = zeroes then
            zr <= '1';
        else
            zr <= '0';
        end if;
    end process; 

    and_xy <= neg_x and neg_y;
end behav;
